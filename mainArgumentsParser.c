//
//  parser.c
//  Ex2Mtm
//
//  Created by Efi Shtain on 11/12/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mainArgumentsParser.h"
#define OPEN_FILE_FOR_READ "r"
#define OPEN_FILE_FOR_WRITE "w"

struct mainArgumentsParser_t{
    int numberOfArguments;
    const char** arguments;
};

MainArgumentsParser mainArgumentsParserCreate(int numberOfArguments, const char** arguments){
    if(numberOfArguments==0||arguments==NULL){
        return NULL;
    }
    MainArgumentsParser parser = malloc(sizeof(*parser));
    if(parser==NULL){
        return NULL;
    }
    parser->numberOfArguments=numberOfArguments;
    parser->arguments=arguments;
    return parser;
}


void mainArgumentsParserDestroy(MainArgumentsParser parser){
    if(parser!=NULL){
        parser->arguments=NULL;
        free(parser);
    }
}

bool isFileExists(const char* filePath){
    FILE* file =fopen(filePath, OPEN_FILE_FOR_READ);
    if(file==NULL){
        return false;
    }
    fclose(file);
    return true;
}

static MainArgumentsParserResult setInputOutputSource(const char* filePath, const char* fileOperation, FILE* destination){
   
//    if(!isFileExists(filePath)){
//        return MAIN_ARGUMENTS_PARSER_CANNOT_OPEN_FILE;
//    }
    
    FILE* file = freopen(filePath, fileOperation, destination);
    if(file==NULL){
        return MAIN_ARGUMENTS_PARSER_CANNOT_OPEN_FILE;
    }
    return MAIN_ARGUMENTS_PARSER_SUCCESS;
}

static MainArgumentsParserResult checkArguments(MainArgumentsParser parser){
    switch (parser->numberOfArguments) {
        case 1:
            return MAIN_ARGUMENTS_PARSER_SUCCESS;
            break;
        case 3:
            if(strcmp(parser->arguments[1], INPUT_FILE_MARKER)==0){
                return setInputOutputSource(parser->arguments[2], OPEN_FILE_FOR_READ,stdin);
            }
            if(strcmp(parser->arguments[1], OUTPUT_FILE_MARKER)==0){
                return setInputOutputSource(parser->arguments[2], OPEN_FILE_FOR_WRITE,stdout);
            }
            return MAIN_ARGUMENTS_PARSER_INVALID_COMMAND_LINE_PARAMETERS;
        case 5:
            if(strcmp(parser->arguments[1],INPUT_FILE_MARKER)==0 && strcmp(parser->arguments[3],OUTPUT_FILE_MARKER)==0){
               MainArgumentsParserResult result =setInputOutputSource(parser->arguments[2], OPEN_FILE_FOR_READ, stdin);
                if(result!=MAIN_ARGUMENTS_PARSER_SUCCESS){
                    return result;
                }
                return setInputOutputSource(parser->arguments[4], OPEN_FILE_FOR_WRITE, stdout);
            }
            if(strcmp(parser->arguments[1],OUTPUT_FILE_MARKER)==0 && strcmp(parser->arguments[3],INPUT_FILE_MARKER)==0){
                MainArgumentsParserResult result = setInputOutputSource(parser->arguments[2], OPEN_FILE_FOR_READ, stdout);
                if(result!=MAIN_ARGUMENTS_PARSER_SUCCESS){
                    return result;
                }
                return setInputOutputSource(parser->arguments[4], OPEN_FILE_FOR_WRITE, stdin);
            }
            return MAIN_ARGUMENTS_PARSER_INVALID_COMMAND_LINE_PARAMETERS;
            
        default:
            return MAIN_ARGUMENTS_PARSER_INVALID_COMMAND_LINE_PARAMETERS;
    }
    
}

MainArgumentsParserResult mainArgumentsParserSetInputOutput(MainArgumentsParser parser){
    if(parser==NULL){
        return MAIN_ARGUEMNTS_PARSER_NULL_ARGUMENT;
    }
    return checkArguments(parser);
}

bool mainArgumentsParserResetArguments(MainArgumentsParser parser,int numberOfArguments,const char** arguments){
    if(parser==NULL||numberOfArguments==0||arguments==NULL){
        return false;
    }
    parser->arguments=arguments;
    parser->numberOfArguments=numberOfArguments;
    return true;
}


