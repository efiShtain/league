#include "mtm_ex2.h"
#include "verify.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>

#define OUTPUT_MAX 20000

void mtmPrintErrorMessage(FILE* errorChannel, MtmErrorCode code) {
	assert(errorChannel != NULL);
	fprintf(errorChannel, "Error: ");
	switch (code) {
	case MTM_OUT_OF_MEMORY:
		fprintf(errorChannel, "Out of memory\n");
		return;
	case MTM_INVALID_COMMAND_LINE_PARAMETERS:
	case MTM_INVALID_PARAMETERS:
		fprintf(errorChannel, "Invalid parameters\n");
		return;
	case MTM_CANNOT_OPEN_FILE:
		fprintf(errorChannel, "Failed to open file\n");
		return;
	case MTM_FORMATION_ALREADY_EXISTS:
		fprintf(errorChannel, "Formation already exists in database\n");
		return;
	case MTM_FORMATION_DOES_NOT_EXIST:
		fprintf(errorChannel, "Formation doesn't exist\n");
		return;
	case MTM_NO_FORMATIONS:
		fprintf(errorChannel, "There are no formations in the database\n");
		return;
	case MTM_FAN_ALREADY_EXISTS:
		fprintf(errorChannel, "A fan with the same ID already appears in database\n");
		return;
	case MTM_FAN_DOES_NOT_EXIST:
		fprintf(errorChannel, "No fan in the system with a specified ID\n");
		return;
	case MTM_NO_FANS:
		fprintf(errorChannel, "There are no fans in the database\n");
		return;
	case MTM_FAN_ALREADY_IN_FORMATION:
		fprintf(errorChannel, "The specified fan already supports the formation\n");
		return;
	case MTM_FAN_NOT_IN_FORMATION:
		fprintf(errorChannel, "The specified fan does not support the formation\n");
		return;
	}
	assert(false);
	return;
}

void mtmPrintFormation(FILE* output, const char* formationName, int formationSize){
	assert(output != NULL);
	fprintf(output,"The formation '%s' has %d fans.\n",formationName,formationSize);
	return;
}

void mtmPrintFan(FILE* output, const char* fanName, int birthyear, int id, int formations){
	assert(output != NULL);
	fprintf(output,"The fan ID %d belongs to %s which is %d years old and supports %d formations.\n",id, fanName, YEAR-birthyear, formations);
	return;
}

void mtmPrintFormationsByAge(FILE* output, int birthyear, const char** formationsNames, int arrayLength){
	assert(output != NULL);

	fprintf(output,"The %d years old fans are supporting the following formations:\n", YEAR-birthyear);
	for (int i=0; i<arrayLength; ++i){
		fprintf(output, "%s", formationsNames[i]);
		fprintf(output, "\n");
	}
	return;
}


