//
//  formation.h
//  Ex2Mtm
//
//  Created by Efi Shtain on 11/13/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//


/**
 * Basic league formation
 *
 * Implements a basic formation in a league, holding formation name, number of fans that
 * support the formation and who are the fans that support the formation
 *
 * The following functions are available:
 *
 *   formationCreate          - Creates a new formation
 *   formationDestroy         - Deletes an existing formation and frees all resources
 *   Formation formationCopy  - Returns a copy of the formation given
 *   formationJoinFan		  - join fan to support the formation
 *   formationRemoveFan		  - remove fan from supporting the formation
 *   formationGetNumberOfFans - Returns number of fans supporting the givan formation
 *   fanCompareFansByAge      - Returns an integer indicating which is bigger
 *
 *   formationGetName         - Returns a copy of the given formation name
 *   formationHasMoreFans     - Returns an integer indicating which formation has more fans
 */


#ifndef Ex2Mtm_formation_h
#define Ex2Mtm_formation_h
#include <stdbool.h>
#include "fan.h"

/** Type for defining the formation */
typedef struct formation_t *Formation;

/** Type used for returning error codes from formation functions */
typedef enum {
    FORMATION_SUCCESS,
    FORMATION_NULL_ARGUMENT,
    FORMATION_OUT_OF_MEMEORY,
    FORMATION_IS_EMPTY,
    FORMATION_FAN_ALREADY_EXIST,
    FORMATION_FAN_NOT_EXIST
}FormationResult;

/**
 * formtionCreate: Creates a new formation
 *
 * @param char* - fan's name
 * @return
 * 	NULL - if the parameter is NULL or allocations failed.
 * 	A new formation in case of success.
 */
Formation formationCreate(char*);

/**
 * formationDestroy: Deletes an existing formation and frees all its resources
 *
 * @param formation - Target formation to be deallocated. If formation is NULL nothing will be
 * 		done
 */
void formationDestroy(Formation);

/**
 * formationCopy: Creates a copy of target formation.
 *
 * @param formation - The target formation to copy
 * @return
 * 	NULL if a NULL was sent or a memory allocation failed.
 * 	An exact copy of the given formation otherwise
 */
Formation formationCopy(Formation);

/**
 * formationJoinFan: joins target fan to support target formation
 *
 * @param formation - the relevant formation to be add to
 * @param fan - the fan to be add
 * @return
 * FORMATION_NULL_ARGUMENT if a NULL was sent or a memory allocation failed.
 * FORMATION_FAN_ALREADY_EXIST if fan already support given formation
 * FORMATION_SUCCESS if fan joined
 *
 */
FormationResult formationJoinFan(Formation , Fan );

/**
 * formationRemoveFan : remove target fan from supporting  given formation
 *
 * @param formation - the relevant formation to be remove from
 * @param fan - fan to be remove
 * @return
 * FORMATION_NULL_ARGUMENT - if a NULL was sent
 * FORMATION_FAN_NOT_EXIST - if fan is not support the formation
 * FORMATION_SUCCESS fan removed
 *
 */
FormationResult formationRemoveFan(Formation , Fan );

/**
 * formationGetNumberOfFans : gets target formation number of fans
 *
 * @param formation- the target formation
 * @return
 * 0 if NULL sent
 * number of formation otherwise
 */
int formationGetNumberOfFans(Formation);

/**
 * formationGetName : gets target formation name
 *
 * @param formation - the relevant formation
 * @return
 * NULL if null agr sent
 * formation name otherwise
 */
char* formationGetName(Formation);

/**
 * formationHasMoreFans : check wich formation has more fans
 *
 * @param formation - first formation
 * @param formation - second formation
 * @return
 * -2 if one of the agr is NULL
 * 1 if first has more fans
 * 0 if equal and
 * -1 if second has more fans
 */
int formationHasMoreFans(Formation,Formation);

/**
 * formationGetFirstFan : returns first fan in formation
 *
 * @param formation - requested formation
 * @return
 * null if a null pointer was sent or formation has no fans
 * first fan otherwise
 */
Fan formationGetFirstFan(Formation);


/**
 * formationGetNextFan : returns next fan in formation
 *
 * @param formation - requested formation
 * @return
 * null if a null pointer was sent or no more fans
 * next fan otherwise
 */
Fan formationGetNextFan(Formation);

#endif
