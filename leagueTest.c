//
//  leagueTest.c
//  LeagueSvn
//
//  Created by Efi Shtain on 12/14/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "league.h"

#define ASSERT(expr) do { \
if(!(expr)) { \
printf("\nAssertion failed %s (%s:%d).\n", #expr, __FILE__, __LINE__); \
return false; \
} else { \
printf("."); \
} \
} while (0)

#define RUN_TEST(test) do { \
printf("Running "#test); \
if(test()) { \
printf("[OK]\n"); \
} \
} while(0)

#define ASSERT_NULL(expr) ASSERT((expr) == LEAGUE_NULL_ARGUMENT)
#define ASSERT_NOT_NULL(expr) ASSERT((expr) != NULL)
#define ASSERT_EQUAL(expr1,expr2) ASSERT ((expr1)==(expr2))
#define ASSERT_TRUE(expr) ASSERT((expr) == true)
#define ASSERT_FALSE(expr) ASSERT((expr) == false)
#define ASSERT_SUCCESS(expr) ASSERT((expr) == LEAGUE_SUCCESS)

static bool testCreateDestroy(){
    League league = leagueCreate();
    ASSERT_NOT_NULL(league);
    leagueDestroy(league);
    league=NULL;
    return true;
}

static bool testAddRemoveFormation(){
    League league = leagueCreate();
    char name[4] = "efi";
    
    ASSERT_NOT_NULL(league);
    //Null tests
    LeagueResult result = leagueAddFormation(NULL, name);
    ASSERT_EQUAL(result, LEAGUE_NULL_ARGUMENT);
    result = leagueAddFormation(league,NULL);
    ASSERT_EQUAL(result, LEAGUE_NULL_ARGUMENT);
    result=leagueRemoveFormation(NULL, name);
    ASSERT_EQUAL(result, LEAGUE_NULL_ARGUMENT);
    result=leagueRemoveFormation(league, NULL);
    ASSERT_EQUAL(result, LEAGUE_NULL_ARGUMENT);
    
    //Test correct error codes
    result = leagueAddFormation(league, name);
    ASSERT_SUCCESS(result);
    result = leagueAddFormation(league, name);
    ASSERT_EQUAL(result, LEAGUE_FORMATION_ALREADY_EXIST);
    //verify not checking casing
    result = leagueAddFormation(league, "Efi");
    ASSERT_SUCCESS(result);
    result=leagueRemoveFormation(league, name);
    ASSERT_SUCCESS(result);
    result=leagueRemoveFormation(league, name);
    ASSERT_EQUAL(result, LEAGUE_FORMATION_NOT_EXIST);
    result = leagueRemoveFormation(league, "Efi");
    ASSERT_SUCCESS(result);
    
    leagueDestroy(league);
    
    return true;
}

bool testAddRemoveFan(){
    League league = leagueCreate();
    char name1[4] = "efi";
    long id1 = 21573621;
    int birthYear1 = 1986;
    
    //Null tests
    LeagueResult result = leagueAddFan(NULL, id1, name1, birthYear1);
    ASSERT_NULL(result);
    result = leagueAddFan(league, id1, NULL, birthYear1);
    ASSERT_NULL(result);
    result = leagueRemoveFan(NULL, id1);
    ASSERT_NULL(result);
    
    //Test correct error codes
    result = leagueAddFan(league, -1, name1, birthYear1);
    ASSERT_EQUAL(result, LEAGUE_INVALID_PARAMETERS);
    result = leagueAddFan(league, id1, name1, -1);
    ASSERT_EQUAL(result, LEAGUE_INVALID_PARAMETERS);
    result = leagueAddFan(league, id1, name1, 2014);
    ASSERT_EQUAL(result, LEAGUE_INVALID_PARAMETERS);
    result = leagueRemoveFan(league, 123);
    ASSERT_EQUAL(result, LEAGUE_FAN_DOES_NOT_EXIST);
    
    result = leagueAddFan(league, id1, name1, birthYear1);
    ASSERT_SUCCESS(result);
    result = leagueAddFan(league, id1, name1, birthYear1);
    ASSERT_EQUAL(result, LEAGUE_FAN_ALREADY_EXIST);
    //check only different ids are needed
    result = leagueAddFan(league, 123, name1, birthYear1);
    ASSERT_SUCCESS(result);
    result = leagueRemoveFan(league, 123);
    ASSERT_SUCCESS(result);
    result = leagueRemoveFan(league, 123);
    ASSERT_EQUAL(result, LEAGUE_FAN_DOES_NOT_EXIST);
    
    leagueDestroy(league);
    
    return true;
}

static bool testAddRemoveFanFormation(){
    League league = leagueCreate();
    
    //null tests
    LeagueResult result = leagueAddFanToFormation(NULL, "efi", 123);
    ASSERT_NULL(result);
    result = leagueAddFanToFormation(league, NULL, 123);
    ASSERT_NULL(result);
    result = leagueRemoveFanFromFormation(NULL, "efi", 123);
    ASSERT_NULL(result);
    result = leagueRemoveFanFromFormation(league, NULL, 123);
    ASSERT_NULL(result);
    
    //check correct error codes
    result = leagueAddFanToFormation(league, "israel", 123);
    ASSERT_EQUAL(result, LEAGUE_FORMATION_NOT_EXIST);
    
    leagueAddFormation(league, "israel");
    result = leagueAddFanToFormation(league, "israel", 123);
    ASSERT_EQUAL(result, LEAGUE_FAN_DOES_NOT_EXIST);
    
    leagueAddFan(league, 123, "efi", 1986);
    result = leagueAddFanToFormation(league, "israel", 123);
    ASSERT_SUCCESS(result);
    result = leagueAddFanToFormation(league, "israel", 123);
    ASSERT_EQUAL(result, LEAGUE_FAN_ALREADY_IN_FORMATION);
    
    result = leagueRemoveFan(league, 123);
    ASSERT_SUCCESS(result);
    result = leagueAddFanToFormation(league, "israel", 123);
    ASSERT_EQUAL(result, LEAGUE_FAN_DOES_NOT_EXIST);
    result = leagueAddFan(league, 123, "efi", 1986);
    ASSERT_SUCCESS(result);
    result = leagueAddFanToFormation(league, "israel", 123);
    ASSERT_SUCCESS(result);
    
    result = leagueRemoveFormation(league, "israel");
    ASSERT_SUCCESS(result);
    result = leagueAddFanToFormation(league, "israel", 123);
    ASSERT_EQUAL(result, LEAGUE_FORMATION_NOT_EXIST);
    
    result = leagueAddFormation(league, "israel");
    ASSERT_SUCCESS(result);
    result = leagueAddFanToFormation(league, "israel", 123);
    ASSERT_SUCCESS(result);
    
    result = leagueRemoveFanFromFormation(league,"israel" ,123);
    ASSERT_SUCCESS(result);
    result = leagueRemoveFanFromFormation(league,"israel" ,123);
    ASSERT_EQUAL(result, LEAGUE_FAN_NOT_IN_FORMATION);
    
    
    char* tempName = malloc(30);
    for (int i=0; i<10; i++) {
        sprintf(tempName,"formation%d",i);
        ASSERT_SUCCESS(leagueAddFormation(league, tempName)) ;
    }
    for(int i=1000;i<1500;i++){
        sprintf(tempName,"fan%d",i);
        ASSERT_SUCCESS( leagueAddFan(league, i, tempName, i));
    }
    
    for (int i=0; i<10; i++) {
        for(int j=1000;j<1050;j++){
            sprintf(tempName, "formation%d",i);
            ASSERT_SUCCESS( leagueAddFanToFormation(league, tempName,j ));
        }
    }
    
    for (int i=0; i<10; i++) {
        for(int j=1000;j<1050;j++){
            sprintf(tempName, "formation%d",i);
            ASSERT_EQUAL( leagueAddFanToFormation(league, tempName,j ),LEAGUE_FAN_ALREADY_IN_FORMATION);
        }
    }
    
    for (int i=4; i<10; i++) {
        for(int j=1010;j<1030;j++){
            sprintf(tempName, "formation%d",i);
            ASSERT_SUCCESS( leagueRemoveFanFromFormation(league, tempName,j ));
        }
    }
    
    for (int i=4; i<10; i++) {
        for(int j=1010;j<1030;j++){
            sprintf(tempName, "formation%d",i);
            ASSERT_EQUAL( leagueRemoveFanFromFormation(league, tempName,j ),LEAGUE_FAN_NOT_IN_FORMATION);
        }
    }
    
    free(tempName);
    leagueDestroy(league);
    return true;
}

static bool reportThreeFormationsFirstCheck(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* formationName = va_arg(ptr, char*);
    int formationSize = va_arg(ptr, int);
    va_end(ptr);
    if(strcmp(formationName, "empty")==0){
        ASSERT_EQUAL(formationSize, 0);
    } else if(strcmp(formationName, "one")==0){
        ASSERT_EQUAL(formationSize, 1);
    }else if(strcmp(formationName, "two")==0){
        ASSERT_EQUAL(formationSize, 2);
    }else{
        ASSERT("Invalid formation name");
    }
    return true;
    
}

static bool reportThreeFormationsSecondCheck(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* formationName = va_arg(ptr, char*);
    int formationSize = va_arg(ptr, int);
    va_end(ptr);
    if(strcmp(formationName, "empty")==0){
        ASSERT_EQUAL(formationSize, 3);
    } else if(strcmp(formationName, "one")==0){
        ASSERT_EQUAL(formationSize, 2);
    }else if(strcmp(formationName, "two")==0){
        ASSERT_EQUAL(formationSize, 1);
    }else{
        ASSERT("Invalid formation name");
    }
    return true;
    
}

static bool reportFormationsOrderFirst(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* formationName = va_arg(ptr, char*);
    int formationSize = va_arg(ptr, int);
    va_end(ptr);
    int res = strcmp(formationName, "empty");
    ASSERT_EQUAL(res, 0);
    ASSERT_EQUAL(formationSize, 3);
    return true;
    
}

static bool reportFormationsOrderSecond(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* formationName = va_arg(ptr, char*);
    int formationSize = va_arg(ptr, int);
    va_end(ptr);
    int res = strcmp(formationName, "threeA");
    ASSERT_EQUAL(res, 0);
    ASSERT_EQUAL(formationSize, 3);
    return true;
    
}

static bool reportFormationsOrderThird(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* formationName = va_arg(ptr, char*);
    int formationSize = va_arg(ptr, int);
    va_end(ptr);
    int res = strcmp(formationName, "one");
    ASSERT_EQUAL(res, 0);
    ASSERT_EQUAL(formationSize, 2);
    return true;
    
}

static bool reportFormationsOrderForth(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* formationName = va_arg(ptr, char*);
    int formationSize = va_arg(ptr, int);
    va_end(ptr);
    int res = strcmp(formationName, "two");
    ASSERT_EQUAL(res, 0);
    ASSERT_EQUAL(formationSize, 1);
    return true;
    
}

bool testReportFormations(){
    League league = leagueCreate();
    LeagueResult result = LEAGUE_SUCCESS;
    
    //Null checks
    result = leagueReportFormations(NULL, 4);
    ASSERT_NULL(result);
    
    //test error codes
    result = leagueReportFormations(league, 2);
    ASSERT_EQUAL(result, LEAGUE_NO_PRINT_CALLBACK);
    leagueSetPrintReportFunction(league, reportThreeFormationsFirstCheck);
    result = leagueReportFormations(league, 0);
    ASSERT_EQUAL(result, LEAGUE_NO_FORMATIONS);
    result = leagueReportFormations(league, -5);
    ASSERT_EQUAL(result, LEAGUE_NO_FORMATIONS);
    
    ASSERT_SUCCESS(leagueAddFormation(league, "empty"));
    ASSERT_SUCCESS(leagueAddFormation(league, "one"));
    ASSERT_SUCCESS(leagueAddFormation(league, "two"));
    
    ASSERT_SUCCESS(leagueAddFan(league, 123, "efi", 1986));
    ASSERT_SUCCESS(leagueAddFan(league, 321, "shtain", 1986));
    
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "one", 123));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "two", 321));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "two", 123));
    
    //Check correct size of formations, 0 for empty, 1 for one and 2 for two
    result = leagueReportFormations(league, 3);
    ASSERT_SUCCESS(result);
    
    leagueSetPrintReportFunction(league, reportThreeFormationsSecondCheck);
    ASSERT_SUCCESS(leagueAddFan(league, 666, "yakir", 1988));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "empty", 666));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "empty", 123));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "empty", 321));
    ASSERT_SUCCESS(leagueRemoveFanFromFormation(league, "two", 321));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "one", 321));
    
    //Check printing after adding and removing fans from formations
    result = leagueReportFormations(league, 3);
    ASSERT_SUCCESS(result);
    leagueAddFormation(league, "threeA");
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "threeA", 666));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "threeA", 123));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "threeA", 321));
    
    leagueSetPrintReportFunction(league, reportFormationsOrderFirst);
    //Check order of report. Report 1 will report only the formation with max number of fans in lexicographic order
    //So empty is expected to re reported
    //After that, it will be removed and test is repeated for threeA, One and finally Two
    result = leagueReportFormations(league, 1);
    ASSERT_SUCCESS(result);
    leagueRemoveFormation(league, "empty");
    
    leagueSetPrintReportFunction(league, reportFormationsOrderSecond);
    result = leagueReportFormations(league, 1);
    ASSERT_SUCCESS(result);
    leagueRemoveFormation(league, "threeA");
    
    leagueSetPrintReportFunction(league, reportFormationsOrderThird);
    result = leagueReportFormations(league, 1);
    ASSERT_SUCCESS(result);
    leagueRemoveFormation(league, "one");
    
    leagueSetPrintReportFunction(league, reportFormationsOrderForth);
    result = leagueReportFormations(league, 1);
    ASSERT_SUCCESS(result);
    leagueRemoveFormation(league, "two");
    
    //All formations have been removed, checking no formation is reported again
    result = leagueReportFormations(league, 3);
    ASSERT_EQUAL(result, LEAGUE_NO_FORMATIONS);
    
    leagueDestroy(league);
    return true;
}

static bool reportOneFanCheck(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* fanName = va_arg(ptr, char*);
    int birthYear = va_arg(ptr, int);
    int id = va_arg(ptr, int);
    int formations = va_arg(ptr, int);
    va_end(ptr);
    int res = strcmp(fanName, "efi");
    ASSERT_EQUAL(res, 0);
    ASSERT_EQUAL(birthYear, 1986);
    ASSERT_EQUAL(id, 1234);
    ASSERT_EQUAL(formations, 0);
    return true;
    
}

static bool reportThreeFanCheckFirstRun(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* fanName = va_arg(ptr, char*);
    int birthYear = va_arg(ptr, int);
    int id = va_arg(ptr, int);
    int formations = va_arg(ptr, int);
    va_end(ptr);
    if(strcmp(fanName, "efi")==0){
        ASSERT_EQUAL(birthYear, 1986);
        ASSERT_EQUAL(id, 1234);
        ASSERT_EQUAL(formations, 0);
    }else if(strcmp(fanName, "yakir")==0){
        ASSERT_EQUAL(birthYear, 1988);
        ASSERT_EQUAL(id, 666);
        ASSERT_EQUAL(formations, 3);
    }else if(strcmp(fanName, "david")){
        ASSERT_EQUAL(birthYear, 2013);
        ASSERT_EQUAL(id, 298);
        ASSERT_EQUAL(formations, 1);
    }
    else{
        ASSERT("Invalid fan");
    }
    
    return true;
    
}

static bool reportThreeFanCheckSecondRun(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* fanName = va_arg(ptr, char*);
    int birthYear = va_arg(ptr, int);
    int id = va_arg(ptr, int);
    int formations = va_arg(ptr, int);
    va_end(ptr);
    if(strcmp(fanName, "efi")==0){
        ASSERT_EQUAL(birthYear, 1986);
        ASSERT_EQUAL(id, 1234);
        ASSERT_EQUAL(formations, 0);
    }else if(strcmp(fanName, "yakir")==0){
        ASSERT_EQUAL(birthYear, 1988);
        ASSERT_EQUAL(id, 666);
        ASSERT_EQUAL(formations, 2);
    }else if(strcmp(fanName, "david")==0){
        ASSERT_EQUAL(birthYear, 2013);
        ASSERT_EQUAL(id, 298);
        ASSERT_EQUAL(formations, 1);
    }
    else{
        ASSERT("Invalid fan");
    }
    
    return true;
    
}

static bool reportOneFanCheckLast(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* fanName = va_arg(ptr, char*);
    int birthYear = va_arg(ptr, int);
    int id = va_arg(ptr, int);
    int formations = va_arg(ptr, int);
    va_end(ptr);
    int res = strcmp(fanName, "david");
    ASSERT_EQUAL(res, 0);
    ASSERT_EQUAL(birthYear, 2013);
    ASSERT_EQUAL(id, 298);
    ASSERT_EQUAL(formations, 0);
    return true;
}

int currentId=0;
static bool reportFansByOrder(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    char* fanName = va_arg(ptr, char*);
    int birthYear = va_arg(ptr, int);
    int id = va_arg(ptr, int);
    int formations = va_arg(ptr, int);
    va_end(ptr);
    
    ASSERT_EQUAL(id, currentId);
    char name[5]={0};
    sprintf(name,"fan%d",currentId);
    int result = strcmp(fanName, name);
    ASSERT_EQUAL(result, 0);
    int year = 1900+currentId;
    ASSERT_EQUAL(year, birthYear);
    ASSERT_EQUAL(formations, 0);
    currentId++;
    return true;
}

bool testReportFans(){
    League league = leagueCreate();
    //test null
    ASSERT_NULL(leagueReportFans(NULL));
    //test error codes
    ASSERT_EQUAL(leagueReportFans(league), LEAGUE_NO_PRINT_CALLBACK);
    
    //test report 1
    leagueSetPrintReportFunction(league, reportOneFanCheck);
    ASSERT_EQUAL(leagueReportFans(league), LEAGUE_NO_FANS);
    leagueAddFan(league, 1234, "efi", 1986);
    ASSERT_SUCCESS(leagueReportFans(league));
    
    //test report 3
    leagueSetPrintReportFunction(league, reportThreeFanCheckFirstRun);
    ASSERT_SUCCESS(leagueAddFan(league, 666, "yakir", 1988));
    ASSERT_SUCCESS(leagueAddFan(league, 298, "david", 2013));
    ASSERT_SUCCESS(leagueAddFormation(league, "one"));
    ASSERT_SUCCESS(leagueAddFormation(league, "two"));
    ASSERT_SUCCESS(leagueAddFormation(league, "three"));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "three", 666));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "one", 666));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "two", 666));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "three", 298));
    ASSERT_SUCCESS(leagueReportFans(league));
    
    leagueSetPrintReportFunction(league, reportThreeFanCheckSecondRun);
    ASSERT_SUCCESS(leagueRemoveFormation(league, "three"));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "two", 298));
    ASSERT_SUCCESS(leagueReportFans(league));
    
    leagueSetPrintReportFunction(league, reportOneFanCheckLast);
    ASSERT_SUCCESS(leagueRemoveFan(league, 666));
    ASSERT_SUCCESS(leagueRemoveFan(league, 1234));
    ASSERT_SUCCESS(leagueRemoveFanFromFormation(league, "two", 298));
    ASSERT_SUCCESS(leagueReportFans(league));
    
    ASSERT_SUCCESS(leagueRemoveFan(league,298));
    ASSERT_EQUAL(leagueReportFans(league), LEAGUE_NO_FANS);
    
    //add fans with random ids between 0 and 9 to league
    //fans are added in randomized order
    int ids[10]={0};
    for(int i=0;i<10;i++){
        int id=rand()%10;
        while(ids[id]==1){
            id=rand()%10;
        }
        ids[id]=1;
        char name[5]={0};
        sprintf(name,"fan%d",id);
        int year = 1900+id;
        ASSERT_SUCCESS(leagueAddFan(league, id, name, year));
    }
    //test fans are reported ordered by id
    leagueSetPrintReportFunction(league, reportFansByOrder);
    ASSERT_SUCCESS(leagueReportFans(league));
    
    leagueDestroy(league);
    return true;
}

int lastAgePrinted = 0;
bool reportFormationsByAgeFirstStage(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    int age =2013- va_arg(ptr, int);
    const char** formationsNames = va_arg(ptr, const char**);
    int arrayLength = va_arg(ptr, int);
    va_end(ptr);
    
    if(age<lastAgePrinted){
        ASSERT("Wrong order of reportFormationByAge");
    }
    lastAgePrinted=age;
    int result=0;
    switch (age) {
        case 0:
            ASSERT_EQUAL(arrayLength, 3);
            result = strcmp("formation1", formationsNames[0]);
            ASSERT_EQUAL(result, 0);
            result = strcmp("formation2", formationsNames[1]);
            ASSERT_EQUAL(result, 0);
            result = strcmp("formation4", formationsNames[2]);
            ASSERT_EQUAL(result, 0);
            break;
            case 20:
            ASSERT_EQUAL(arrayLength, 1);
            result = strcmp("formation3", formationsNames[0]);
            ASSERT_EQUAL(result, 0);
            break;
            case 30:
            ASSERT_EQUAL(arrayLength, 6);
            result = strcmp("formation1", formationsNames[0]);
            ASSERT_EQUAL(result, 0);
            result = strcmp("formation2", formationsNames[1]);
            ASSERT_EQUAL(result, 0);
            result = strcmp("formation3", formationsNames[2]);
            ASSERT_EQUAL(result, 0);
            result = strcmp("formation4", formationsNames[3]);
            ASSERT_EQUAL(result, 0);
            result = strcmp("formation5", formationsNames[4]);
            ASSERT_EQUAL(result, 0);
            result = strcmp("formation6", formationsNames[5]);
            ASSERT_EQUAL(result, 0);
            break;
        case 57:
            ASSERT_EQUAL(arrayLength, 0);
            break;
        default:
            ASSERT("Invalid age, an error occoured");
    }
    return true;
}

bool reportFormationsByAgeSecondStage(ReportType type,...){
    va_list ptr;
    va_start(ptr, type);
    int age =2013- va_arg(ptr, int);
    const char** formationsNames = va_arg(ptr, const char**);
    int arrayLength = va_arg(ptr, int);
    va_end(ptr);
    
    if(age<lastAgePrinted){
        ASSERT("Wrong order of reportFormationByAge");
    }
    lastAgePrinted=age;
    int result=0;
    switch (age) {
        case 0:
            ASSERT_EQUAL(arrayLength, 2);
            result = strcmp("formation1", formationsNames[0]);
            ASSERT_EQUAL(result, 0);
            result = strcmp("formation2", formationsNames[1]);
            ASSERT_EQUAL(result, 0);
            break;
        case 20:
            ASSERT_EQUAL(arrayLength, 1);
            result = strcmp("formation3", formationsNames[0]);
            ASSERT_EQUAL(result, 0);
            break;
        case 30:
            ASSERT_EQUAL(arrayLength, 2);
            result = strcmp("formation5", formationsNames[0]);
            ASSERT_EQUAL(result, 0);
            result = strcmp("formation6", formationsNames[1]);
            ASSERT_EQUAL(result, 0);
            break;
        case 57:
            ASSERT_EQUAL(arrayLength, 1);
            result = strcmp("formation2", formationsNames[0]);
            ASSERT_EQUAL(result, 0);
            break;
        default:
            ASSERT("Invalid age, an error occoured");
    }
    return true;
}

bool testReportFormationsByAge(){
    League league = leagueCreate();
    //null checks
    ASSERT_NULL(leagueReportFormationsByAge(NULL));
    
    //test error codes
    ASSERT_EQUAL(leagueReportFormationsByAge(league), LEAGUE_NO_PRINT_CALLBACK);
    leagueSetPrintReportFunction(league, reportFormationsByAgeFirstStage);
    ASSERT_EQUAL(leagueReportFormationsByAge(league), LEAGUE_NO_FORMATIONS);
    ASSERT_SUCCESS(leagueAddFormation(league, "formation1"));
    ASSERT_EQUAL(leagueReportFormationsByAge(league), LEAGUE_NO_FANS);
    
    //add 6 formations to league
    for(int i=2;i<=6;i++){
        char name[12];
        sprintf(name, "formation%d",i);
        ASSERT_SUCCESS(leagueAddFormation(league, name));
    }
    
    ASSERT_SUCCESS(leagueAddFan(league, 0, "fan0", 2013));
    ASSERT_SUCCESS(leagueAddFan(league, 1, "fan1", 2013));
    ASSERT_SUCCESS(leagueAddFan(league, 2, "fan2", 1993));
    ASSERT_SUCCESS(leagueAddFan(league, 3, "fan3", 1983));
    ASSERT_SUCCESS(leagueAddFan(league, 4, "fan4", 1956));
    ASSERT_SUCCESS(leagueAddFan(league, 5, "fan5", 1983));
    
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation1", 0));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation1", 1));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation1", 5));
    
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation2", 0));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation2", 5));
    
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation3", 2));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation3", 5));
    
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation4", 1));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation4", 3));
    
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation5", 3));
    
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation6", 3));
    ASSERT_SUCCESS(leagueReportFormationsByAge(league));
    
    
    ASSERT_SUCCESS(leagueRemoveFormation(league, "formation4"));
    ASSERT_SUCCESS(leagueRemoveFan(league, 1));
    ASSERT_SUCCESS(leagueRemoveFan(league, 5));
    ASSERT_SUCCESS(leagueAddFanToFormation(league, "formation2", 4));
    leagueSetPrintReportFunction(league, reportFormationsByAgeSecondStage);
    lastAgePrinted=0;
    ASSERT_SUCCESS(leagueReportFormationsByAge(league));
    
    ASSERT_SUCCESS(leagueRemoveFan(league, 0));
    ASSERT_SUCCESS(leagueRemoveFan(league, 2));
    ASSERT_SUCCESS(leagueRemoveFan(league, 3));
    ASSERT_SUCCESS(leagueRemoveFan(league, 4));
    
    ASSERT_EQUAL(leagueReportFormationsByAge(league), LEAGUE_NO_FANS);
    
    ASSERT_SUCCESS(leagueRemoveFormation(league, "formation1"));
    ASSERT_SUCCESS(leagueRemoveFormation(league, "formation2"));
    ASSERT_SUCCESS(leagueRemoveFormation(league, "formation3"));
    ASSERT_SUCCESS(leagueRemoveFormation(league, "formation5"));
    ASSERT_SUCCESS(leagueRemoveFormation(league, "formation6"));
    
    ASSERT_EQUAL(leagueReportFormationsByAge(league), LEAGUE_NO_FORMATIONS);
    
    leagueDestroy(league);
    
    
    return true;
}

//int main(int argc, char** argv){
//    RUN_TEST(testCreateDestroy);
//    RUN_TEST(testAddRemoveFormation);
//    RUN_TEST(testAddRemoveFan);
//    RUN_TEST(testAddRemoveFanFormation);
//    RUN_TEST(testReportFormations);
//    RUN_TEST(testReportFans);
//    RUN_TEST(testReportFormationsByAge);
//}
