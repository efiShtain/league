
//
//  league.c
//  League
//
//  Created by Efi Shtain on 11/14/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "league.h"
#include "map.h"
#include "list.h"
#include "fan.h"
#include "formation.h"



///************************
///League struct definition
///************************
struct league_t {
	Map fanFormationMap;
	Map formationsMap;
	Map fansMap;
    PrintFunc printCallback;
};


///*************************************************************************************************************************************
///Aid functions for maps
///*************************************************************************************************************************************

//*******************************************************
//FansMap
//*******************************************************
typedef void* ContainerElement;
ContainerElement getOriginalValue(ContainerElement data){
    return data;
}

void freeSimpleType(ContainerElement element){
    free(element);
}

MapKeyElement copyLongElement(MapKeyElement key){
    if(key==NULL){
        return NULL;
    }
    long* copyKey = malloc(sizeof(*copyKey));
    if (copyKey==NULL) {
        return NULL;
    }
    *copyKey=*(long*)key;
    return (MapKeyElement)copyKey;
    //    return (long*)key;
}


void freeFanElement(MapDataElement data){
    fanDestroy((Fan)data);
}



int compareStringElements(MapKeyElement key1, MapKeyElement key2){
    return strcmp((char*)key1, (char*)key2);
}

MapKeyElement copyStringElement(MapKeyElement key){
    if(key==NULL){
        return NULL;
    }
    char* copyKey = malloc(strlen((char*)key)+1);
    if (copyKey==NULL) {
        return NULL;
    }
    strcpy(copyKey, (char*)key);
    return (MapKeyElement)copyKey;
}

void freeFanFormationDataElement(MapDataElement data){
    listDestroy((List)data);
}

int compareFansById(ContainerElement key1, ContainerElement key2){
    if(key1==NULL||key2==NULL){
        return -2;
    }
    if(*(long*)key1>*(long*)key2){
        return 1;
    }
    if(*(long*)key1<*(long*)key2){
        return -1;
    }
    return 0;
}


void freeFormationElement(ContainerElement element){
    formationDestroy(element);
}

void dummyFreeFunction(ListElement element){
    return;
}


///*************************************************************************************************************************************
///Converters
///*************************************************************************************************************************************

typedef enum {
    FORMATION,
    FAN
}MapResultRelevancy;

LeagueResult mapResultToLeagueResultConverter(MapResult result,MapResultRelevancy relevancy){
    switch (result) {
        case MAP_ITEM_ALREADY_EXISTS:
            if (relevancy==FORMATION) {
                return LEAGUE_FORMATION_ALREADY_EXIST;
            }
            return LEAGUE_FAN_ALREADY_EXIST;
        case MAP_ITEM_DOES_NOT_EXIST:
            if (relevancy==FORMATION) {
                return LEAGUE_FORMATION_NOT_EXIST;
            }
            return LEAGUE_FAN_DOES_NOT_EXIST;
        case MAP_NULL_ARGUMENT:
            return LEAGUE_NULL_ARGUMENT;
        case MAP_OUT_OF_MEMORY:
            return LEAGUE_MEMEORY_ERROR;
        default:
            return LEAGUE_SUCCESS;
    }
}

LeagueResult formationResultToLeagueResultConverter(FormationResult result){
    switch (result) {
        case FORMATION_NULL_ARGUMENT:
            return LEAGUE_NULL_ARGUMENT;
        case FORMATION_OUT_OF_MEMEORY:
            return LEAGUE_MEMEORY_ERROR;
        case FORMATION_IS_EMPTY:
            return  LEAGUE_FAN_NOT_IN_FORMATION;
        case FORMATION_FAN_NOT_EXIST:
            return LEAGUE_FAN_NOT_IN_FORMATION;
        case FORMATION_FAN_ALREADY_EXIST:
            return LEAGUE_FAN_ALREADY_IN_FORMATION;
        default:
            return LEAGUE_SUCCESS;
    }
}


///*************************************************************************************************************************************
///Ctor, Destroy
///*************************************************************************************************************************************
League leagueCreate(){
	League league=malloc(sizeof(*league));
	if (league==NULL){
		return NULL;
	}
	league->fanFormationMap = NULL;
	league->formationsMap=NULL;
	league->fansMap=NULL;
	
	league->fanFormationMap= mapCreate(getOriginalValue, copyLongElement, freeFanFormationDataElement, freeSimpleType, compareFansById);
	if (league->fanFormationMap==NULL) {
		leagueDestroy(league);
		return NULL;
	}
	league->fansMap = mapCreate(getOriginalValue,copyLongElement,freeFanElement,freeSimpleType,compareFansById);
	if (league->fansMap==NULL) {
		leagueDestroy(league);
        return NULL;
    }
	league->formationsMap = mapCreate(getOriginalValue,copyStringElement,freeFormationElement,freeSimpleType,compareStringElements);
	if (league->formationsMap==NULL) {
		leagueDestroy(league);
        return NULL;
    }
    league->printCallback=NULL;
	return league;
}

void leagueDestroy(League league) {
	if (league!=NULL){
		mapDestroy(league->formationsMap);
        mapDestroy(league->fansMap);
		mapDestroy(league->fanFormationMap);
		free(league);
	}
}

LeagueResult leagueSetPrintReportFunction(League league,PrintFunc callback){
    if(league==NULL||callback==NULL){
        return LEAGUE_NULL_ARGUMENT;
    }
    league->printCallback=callback;
    return LEAGUE_SUCCESS;
}

///**********************************************************************
///League aid functions
///**********************************************************************

List mapToList(Map map, CompareListElements requestedSort ){
    if(map==NULL){
        return NULL;
    }
    
    List newList = listCreate(getOriginalValue,dummyFreeFunction);
    MapResult result;
    MAP_FOREACH(ListElement, element, map, &result){
        listInsertLast(newList, mapGet(map, element));
        free(element);
    }
    if(requestedSort!=NULL){
        listSort(newList, requestedSort);
    }
    return newList;
}

///**********************************************************************
///League interface functions
///**********************************************************************

//**************************************************************************************************************************************
//League Add\Remove formation
//**************************************************************************************************************************************
LeagueResult leagueAddFormation(League league,char* name){
    
    if(league==NULL||name==NULL){
        
        return LEAGUE_NULL_ARGUMENT;
    }
    
    if(mapContains(league->formationsMap, name)){
        return LEAGUE_FORMATION_ALREADY_EXIST;
    }
    
    Formation newFormation = formationCreate(name);
    if(newFormation==NULL){
        return LEAGUE_MEMEORY_ERROR;
    }
    
    MapResult result =mapPut(league->formationsMap, name, newFormation);
    return mapResultToLeagueResultConverter(result,FORMATION);
}

LeagueResult findAndRemoveFormationNameInList(List formationsList, Formation formation){
    
    LIST_FOREACH(Formation, formationPtr, formationsList){
        if(formationPtr==formation){
            listRemoveCurrent(formationsList);
            break;
        }
    }
    return LEAGUE_SUCCESS;
}

LeagueResult removeFormationFromFanFormationsMap(Map fanFormationsMap, Formation formation){
    
    MapResult result;
    MAP_FOREACH(long*, fanId, fanFormationsMap,&result){
        List formationsList = mapGet(fanFormationsMap, fanId);
        if(formationsList!=NULL){
            findAndRemoveFormationNameInList(formationsList,formation);
        }
        free(fanId);
    }
    return LEAGUE_SUCCESS;
}

LeagueResult leagueRemoveFormation(League league,char* name){
    
    if(league==NULL||name==NULL){
        return LEAGUE_NULL_ARGUMENT;
    }
    if(!mapContains(league->formationsMap, name)){
        return LEAGUE_FORMATION_NOT_EXIST;
    }
    Formation formation = mapGet(league->formationsMap, name);
    removeFormationFromFanFormationsMap(league->fanFormationMap,formation);
    MapResult result = mapRemove(league->formationsMap, name);
    return mapResultToLeagueResultConverter( result,FORMATION);
}



//**************************************************************************************************************************************
//League report formations
//**************************************************************************************************************************************

int formationsListSortByName(ListElement element1, ListElement element2){
    return strcmp(formationGetName(element1), formationGetName(element2));
}

int formationsListCompareByNumberOfFans(ListElement element1, ListElement element2){
    return formationHasMoreFans((Formation)element1, (Formation)element2);
}

//check what happens if one of the argument is null
int formationsListLexicographicCompareFunction(ListElement element1,ListElement element2){
    if(element1==NULL||element2==NULL){
        return -2;
    }
    return strcmp(formationGetName((Formation)element1), formationGetName((Formation)element2));
    
}

void sortByLexicographicAndPrintList(List list, int numberOfFans,int printLimit,PrintFunc printCallback){
    listSort(list, formationsListLexicographicCompareFunction);
    int counter=0;
    LIST_FOREACH(Formation, formation, list){
        //mtmPrintFormation(stdout,formationGetName(formation),numberOfFans);
        if(counter<printLimit){
            printCallback(REPORT_FORMATIONS,formationGetName(formation),numberOfFans);
            counter++;
        }
    }
}

//LeagueResult printFormationsByNumberOfFans(List formationsList, int count, PrintFunc printCallback){
//    List tempList = listCreate(getOriginalValue, dummyFreeFunction);
//    if(tempList==NULL){
//        return LEAGUE_MEMEORY_ERROR;
//    }
//
//    int i=0;
//    int numberOfFans = formationGetNumberOfFans((Formation)listGetFirst(formationsList));
//
//    LIST_FOREACH(Formation, formation, formationsList){
//        int currentNumberOfFansInFormation =formationGetNumberOfFans(formation);
//        if(i==count){
//            break;
//        }
//        if(numberOfFans!=currentNumberOfFansInFormation){
//
//            sortByLexicographicAndPrintList(tempList,numberOfFans,count,printCallback);
//
//            listClear(tempList);
//            listInsertLast(tempList, formation);
//            numberOfFans = formationGetNumberOfFans(formation);
//
//
//        }else{
//
//            listInsertLast(tempList, formation);
//        }
//        i++;
//    }
//
//    sortByLexicographicAndPrintList(tempList, numberOfFans,count,printCallback);
//
//    listDestroy(tempList);
//    tempList=NULL;
//    return LEAGUE_SUCCESS;
//
//}

LeagueResult printFormationsByNumberOfFans(List formationsList, int count, PrintFunc printCallback){
    List tempList = listCreate(getOriginalValue, dummyFreeFunction);
    if(tempList==NULL){
        return LEAGUE_MEMEORY_ERROR;
    }
    
    int i=0,printedTillNow=0;
    
    Formation formation = listGetFirst(formationsList);
    int numberOfFans = formationGetNumberOfFans(formation);
    
    while(formation!=NULL){
        if(numberOfFans==formationGetNumberOfFans(formation)){
            listInsertLast(tempList, formation);
            formation=listGetNext(formationsList);
        }else{
            sortByLexicographicAndPrintList(tempList, numberOfFans, count, printCallback);
            printedTillNow+=i-printedTillNow;
            listClear(tempList);
            if(i>=count){
                break;
            }
            numberOfFans=formationGetNumberOfFans(formation);
            listInsertLast(tempList, formation);
            formation=listGetNext(formationsList);
        }
        i++;
    }
    sortByLexicographicAndPrintList(tempList, numberOfFans, count-printedTillNow, printCallback);
    listDestroy(tempList);
    tempList=NULL;
    formation=NULL;
    return LEAGUE_SUCCESS;
    
}

LeagueResult leagueReportFormations(League league,int count){
    if(league==NULL){
        return LEAGUE_NULL_ARGUMENT;
    }
    if(league->printCallback==NULL){
        return LEAGUE_NO_PRINT_CALLBACK;
    }
    //Check if it is success or failure?
    //What should be done here?
//    if (count==0) {
//        return LEAGUE_SUCCESS;
//    }
    if(mapGetSize(league->formationsMap)==0){
        return LEAGUE_NO_FORMATIONS;
    }
    List formationsList = mapToList(league->formationsMap,formationsListSortByName);
    listSort(formationsList, formationsListCompareByNumberOfFans);
    LeagueResult result= printFormationsByNumberOfFans(formationsList,count,league->printCallback);
    listDestroy(formationsList);
    formationsList=NULL;
    return result;
}



//**************************************************************************************************************************************
//League Add\Remove Fan
//**************************************************************************************************************************************
LeagueResult leagueAddFan(League league,long id,char* name,int yearOfBirth){
    if(league==NULL||name==NULL){
        return LEAGUE_NULL_ARGUMENT;
    }
    if(mapContains(league->fansMap, &id)){
        return LEAGUE_FAN_ALREADY_EXIST;
    }
    if(yearOfBirth<MIN_YEAR_OF_BIRTH|| yearOfBirth>MAX_YEAR_OF_BIRTH||id<0){
        return LEAGUE_INVALID_PARAMETERS;
    }
    
    Fan fan = fanCreate(id, name, yearOfBirth);
    if(fan==NULL){
        return LEAGUE_MEMEORY_ERROR;
    }
    
    return mapResultToLeagueResultConverter( mapPut(league->fansMap, &id, fan),FAN);
    
}

LeagueResult leagueRemoveFan(League league,long id){
    if(league==NULL){
        return LEAGUE_NULL_ARGUMENT;
    }
//    if(id<0){
//        return LEAGUE_INVALID_PARAMETERS;
//    }
    Fan fan = mapGet(league->fansMap,&id);
    if(fan==NULL){
    	return LEAGUE_FAN_DOES_NOT_EXIST;
    }
    
    List formationList = mapGet(league->fanFormationMap,&id);
    if (formationList!=NULL) {
    	LIST_FOREACH(Formation,formation,formationList){
            formationRemoveFan(formation,fan);
    	}
    	mapRemove(league->fanFormationMap,&id);
    }
    return mapResultToLeagueResultConverter(mapRemove(league->fansMap, &id),FAN);
}




//**************************************************************************************************************************************
//League Add\Remove Fan from formation
//**************************************************************************************************************************************
LeagueResult leagueAddFanToFormation(League league,char* formationName,long fanId){
    if (league==NULL||formationName==NULL) {
        return LEAGUE_NULL_ARGUMENT;
    }
//    if(fanId<0){
//        return LEAGUE_INVALID_PARAMETERS;
//    }
    Formation formation =mapGet(league->formationsMap, formationName);
    if(formation==NULL){
        return LEAGUE_FORMATION_NOT_EXIST;
    }
    Fan fan = mapGet(league->fansMap, &fanId);
    if(fan==NULL){
        return LEAGUE_FAN_DOES_NOT_EXIST;
    }
    FormationResult result =  formationJoinFan(formation,fan);
    if(result==FORMATION_SUCCESS){
        
        List fanFormationsList = mapGet(league->fanFormationMap, &fanId);
        if (fanFormationsList==NULL) {
            fanFormationsList=listCreate(getOriginalValue, dummyFreeFunction);
            mapPut(league->fanFormationMap, &fanId, fanFormationsList);
        }
        listInsertFirst(fanFormationsList, formation);
    }
    return formationResultToLeagueResultConverter(result);
}
LeagueResult leagueRemoveFanFromFormation(League league,char* formationName,long fanId){
    if (league==NULL||formationName==NULL) {
        return LEAGUE_NULL_ARGUMENT;
    }
//    if(fanId<0){
//        return LEAGUE_INVALID_PARAMETERS;
//    }
    Formation formation =mapGet(league->formationsMap, formationName);
    if(formation==NULL){
        return LEAGUE_FORMATION_NOT_EXIST;
    }
    Fan fan =  mapGet(league->fansMap, &fanId);
    if(fan==NULL){
        return LEAGUE_FAN_DOES_NOT_EXIST;
    }
    FormationResult result =  formationRemoveFan(formation,fan);
    if(result==FORMATION_SUCCESS){
        
        
        List fanFormationsList = mapGet(league->fanFormationMap, &fanId);
        if(fanFormationsList!=NULL){
            LIST_FOREACH(Formation, formation, fanFormationsList){
                if(strcmp(formationGetName(formation),formationName)==0){
                    listRemoveCurrent(fanFormationsList);
                    break;
                }
            }
        }
    }
    return formationResultToLeagueResultConverter(result);
}

//**************************************************************************************************************************************
//League report formation by age
//**************************************************************************************************************************************



int SortListByFanAgeFunction (ListElement element1, ListElement element2){
    return (-1)*(fanCompareFansByAge((Fan)element1,(Fan)element2));
    
}

int tempMapComperFunction(MapKeyElement element1, MapKeyElement element2){
    if(element1==NULL||element2==NULL){
        return -2;
    }
    return strcmp((char*)element1,(char*)element2);
}
char** mapToStringArrayFunction (Map map) {
	char** names = malloc(mapGetSize(map)*sizeof(char*));
	if (names== NULL) {
		return NULL;
	}
	int i=0;
    MapResult result;
	MAP_FOREACH(char*,formationName,map,&result){
        names[i]= malloc(strlen(formationName)+1);
        if (names[i]== NULL) {
            for (int j=0;j<i;j++) {
                free(names[j]);
                names[j]=NULL;
            }
            free(names);
            names=NULL;
            return NULL;
        }
        strcpy(names[i],formationName);
        i++;
        //free(formationName);
	}
	return  names;
}

void destroyStringsArray ( char** array, int arrayLen){
	if(array!=NULL) {
		for (int i=0 ; i < arrayLen ; i++){
			free(array[i]);
			array[i]=NULL;
		}
		free(array);
	}
	return ;
}
//*******************************************************
//EndOfFunctionsForReportFansByAge
//*******************************************************


LeagueResult leagueReportFormationsByAge(League league){
    if (league==NULL) {
    	return LEAGUE_NULL_ARGUMENT ;
    }
    
    if(league->printCallback==NULL){
        return LEAGUE_NO_PRINT_CALLBACK;
    }
    
	if (mapGetSize(league->formationsMap)== 0) {
		return LEAGUE_NO_FORMATIONS;
    }
	if (mapGetSize(league->fansMap)==0) {
		return LEAGUE_NO_FANS;
	}
    
	List fansListSortedByAge = mapToList(league->fansMap,compareFansById);
	listSort(fansListSortedByAge,SortListByFanAgeFunction);
	Map tempMap = mapCreate(getOriginalValue,getOriginalValue,dummyFreeFunction,dummyFreeFunction,tempMapComperFunction);
	int numberOfFormation=0;
	int currentYear = fanGetDateOfBirth(listGetFirst(fansListSortedByAge));
    char** formationNames=NULL;
	LIST_FOREACH(Fan,fan,fansListSortedByAge){
		long fanId = fanGetId(fan);
		if(fanGetDateOfBirth(fan)!=currentYear){
            formationNames = mapToStringArrayFunction(tempMap);
            //mtmPrintFormationsByAge(stdout,currentYear,(const char**)formationNames,numberOfFormation);
            league->printCallback(REPORT_FORMATIONS_BY_AGE,currentYear,formationNames,numberOfFormation);
            destroyStringsArray(formationNames, numberOfFormation);
            formationNames=NULL;
            currentYear=fanGetDateOfBirth(fan);
            mapClear(tempMap);
            numberOfFormation=0;
            List listOfFormations = mapGet(league->fanFormationMap,&fanId);
            LIST_FOREACH(Formation,formation,listOfFormations) {
                
                mapPut(tempMap,formationGetName(formation),formation);
                numberOfFormation++;
            }
            
		}
		else{
			MapDataElement listOfFormations = mapGet(league->fanFormationMap,&fanId);
            MapResult result;
			LIST_FOREACH(Formation,formation,listOfFormations) {
                char* formationName = formationGetName(formation);
                result = mapPut(tempMap,formationName,formation);
                if(result==MAP_SUCCESS_ITEM_ADDED){
                    numberOfFormation++;
                }
                
			}
            
		}
	}
    formationNames = mapToStringArrayFunction(tempMap);
    //mtmPrintFormationsByAge(stdout,currentYear,(const char**)formationNames,numberOfFormation);
    league->printCallback(REPORT_FORMATIONS_BY_AGE,currentYear,formationNames,numberOfFormation);
    destroyStringsArray(formationNames, numberOfFormation);
    formationNames=NULL;
    mapDestroy(tempMap);
    tempMap=NULL;
    listDestroy(fansListSortedByAge);
    fansListSortedByAge=NULL;
    return LEAGUE_SUCCESS;
}


//**************************************************************************************************************************************
//League report fans
//**************************************************************************************************************************************
LeagueResult leagueReportFans(League league){
    if (league==NULL ) {
        return LEAGUE_NULL_ARGUMENT ;
    }
    if(league->printCallback==NULL){
        return LEAGUE_NO_PRINT_CALLBACK;
    }
	if (mapGetSize(league->fansMap)==0){
		return LEAGUE_NO_FANS;
	}
    MapResult result;
	MAP_FOREACH(long*,id,league->fansMap,&result){
        
        
		List list = mapGet(league->fanFormationMap,id);
		int numberOfFormations = listGetSize(list);
		if (numberOfFormations==-1){
			numberOfFormations=0;
		}
		Fan fan =mapGet(league->fansMap,id);
        char* fanName = fanGetName(fan);
		//mtmPrintFan(stdout,fanName,fanGetDateOfBirth(fan),*(int*)id,numberOfFormations );
        league->printCallback(REPORT_FANS,fanName,fanGetDateOfBirth(fan),*(int*)id,numberOfFormations);
        free(fanName);
        free(id);
	}
    return LEAGUE_SUCCESS;
}


