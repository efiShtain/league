//
//  parser.h
//  Ex2Mtm
//
//  Created by Efi Shtain on 11/12/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#ifndef Ex2Mtm_parser_h
#define Ex2Mtm_parser_h
#include <stdbool.h>

/**
 * flags indicating an input file and an outfile
 */
#define INPUT_FILE_MARKER "-i"
#define OUTPUT_FILE_MARKER "-o"

/**
 * Main arguments parser
 *
 * Implements a parser which check the input arguments and sets the stdin and stdout accrodingly
 *
 * The following functions are available:
 *
 *   mainArgumentsParserCreate               - Creates a new parser
 *   mainArgumentsParserDestroy              - Deletes an existing parser and frees all resources
 *   mainArgumentsParserSetInputOutput       - Parser the arguments and sets the input and output
 *   mainArgumentsParserResetArguments       - Resets the main arguments, after this 
                                             - mainArgumentsParserSetInputOutput shall be called again
 */

/** Type for defining the fan */
typedef struct mainArgumentsParser_t *MainArgumentsParser;

/** Type used for returning error codes from parser functions */
typedef enum InputOutOperations_t {
    MAIN_ARGUMENTS_PARSER_SUCCESS,
    MAIN_ARGUMENTS_PARSER_INVALID_COMMAND_LINE_PARAMETERS,
    MAIN_ARGUMENTS_PARSER_CANNOT_OPEN_FILE,
    MAIN_ARGUMENTS_PARSER_MEMORY_ERROR,
    MAIN_ARGUEMNTS_PARSER_NULL_ARGUMENT
}MainArgumentsParserResult;

/**
 * mainArgumentsParserCreate: Creates a new main arguments parser
 *
 * @param int - number of arguments
 * @param char** - array of arguments
 * @return
 * 	NULL - if arguments is NULL or number of arguments is 0 or allocations failed.
 * 	A new parser in case of success.
 */
MainArgumentsParser mainArgumentsParserCreate(int, const char**);

/**
 * mainArgumentsParserDestroy: Deletes an existing parser and frees all its resources
 *
 * @param MainArgumentsParser - Target parser to be deallocated. If parser is NULL nothing will be
 * 		done
 */
void mainArgumentsParserDestroy(MainArgumentsParser);

/**
 * mainArgumentsParserSetInputOutput: parser the arguments array and decides from where to read input
 *                                    and to where it should write the output
 *
 * @param MainArgumentsParser - Target parser. 
 * @return
 * 	MAIN_ARGUEMNTS_PARSER_NULL_ARGUMENT if parser is null
 *  MAIN_ARGUMENTS_PARSER_CANNOT_OPEN_FILE if an input or output file was set and cannot be open
 *  MAIN_ARGUMENTS_PARSER_INVALID_COMMAND_LINE_PARAMETERS if arguments format is not valid
 *  MAIN_ARGUMENTS_PARSER_SUCCESS in any other case
 */
MainArgumentsParserResult mainArgumentsParserSetInputOutput(MainArgumentsParser);

/**
 * mainArgumentsParserResetArguments: Resets the number of arguments and arguments array to parse
 *
 * @param MainArgumentsParser - Target parser.
 * @return
 * 	false if parser is null, number of arguments is 0 or arguments array is null
 *  true in other case
 */
bool mainArgumentsParserResetArguments(MainArgumentsParser,int,const char**);
#endif
