//
//  IOWriter.h
//  League
//
//  Created by Efi Shtain on 11/25/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//Chnaged writer

#ifndef League_IOWriter_h
#define League_IOWriter_h

#include "mtm_ex2.h"
#include "functionParser.h"
#include <stdbool.h>

typedef struct ioWriter_t *IOWriter;
typedef enum{
    IO_DONE,
    IO_NULL_ARGUMENT,
    IO_MEMORY_ERROR,
    IO_FAILED,
    IO_INVALID_COMMAND
}IOWriterResult;

//IOWriter IOWriterCreate(FunctionParser);
IOWriter IOWriterCreate(League);
void IOWriterDestroy(IOWriter);
IOWriterResult IOWriterBegin(IOWriter);
#endif
