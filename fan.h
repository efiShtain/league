//
//  Fan.h
//  Ex2Mtm
//
//  Created by Efi Shtain on 11/13/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#ifndef Ex2Mtm_fan_h
#define Ex2Mtm_fan_h

/**
 *Minimum and maximum allowed year of birth of a fan
 */
#define MIN_YEAR_OF_BIRTH 0
#define MAX_YEAR_OF_BIRTH 2013

/**
 * Basic league fan
 *
 * Implements a basic fan in a league, holding his\her id, name and year of birth
 *
 * The following functions are available:
 *
 *   fanCreate               - Creates a new fan
 *   fanDestroy              - Deletes an existing fan and frees all resources
 *   fanCopy                 - Copies an existing fan
 *   fanGetId                - Returns the id of a given fan
 *   fanGetName              - Returns a new copy of given fan's name
 *   fanGetDateOfBirth       - Returns given fan's year of birth
 *   fanCompareFansByAge     - Returns an integer indicating which is bigger
 *                           - between two given fans
 *   fanCompareFansById      - Returns an integer indicating which id is higher 
 *                           - between two given fans
 */

/** Type for defining the fan */
typedef struct fan_t *Fan;

/** Type used for returning error codes from fan functions */ //Is this needed? we don't use it in any place
typedef enum{
    FAN_SUCCESS,
    FAN_NULL_ARGUMENT,
    FAN_OUT_OF_MEMEORY,
    FAN_INVALID_YEAR_OF_BIRTH
}FanResult;

/**
 * fanCreate: Creates a new fan
 *
 * @param long - fan's id
 * @param char* - fan's name
 * @param int - fan's year of birth, cannot be greater than MAX_YEAR_OF_BIRTH
                or smaller than MIN_YEAR_OF_BIRTH
 * @return
 * 	NULL - if one of the parameters is NULL or allocations failed.
 * 	A new fan in case of success.
 */
Fan fanCreate(long,char*,int);

/**
 * fanDestroy: Deletes an existing fan and frees all its resources
 *
 * @param fan - Target fan to be deallocated. If fan is NULL nothing will be
 * 		done
 */
void fanDestroy(Fan);

/**
 * fanCopy: Creates a copy of target fan.
 *
 * @param fan - The target fan to copy
 * @return
 * 	NULL if a NULL was sent or a memory allocation failed.
 * 	An exact copy of the given fan otherwise
 */
Fan fanCopy(Fan);

/**
 * fanGetId: Returns the id of a given fan
 * @param fan - The target fan which id is requested.
 * @return
 * 	-1 if a NULL pointer was sent.
 * 	Otherwise the id of given fan.
 */
long fanGetId(Fan);

/**
 * fanGetName: Returns a copy of a given fan's name
 * @param fan - The target fan which name is requested.
 * @return
 * 	NULL if a NULL pointer was sent or memeory allocation failed.
 * 	Otherwise a copy of the name of the given fan.
 */
char* fanGetName(Fan);

/**
 * fanGetDateOfBirth: Returns the year of birth of a given fan
 * @param fan - The target fan which year of birth is requested.
 * @return
 * 	-1 if a NULL pointer was sent.
 * 	Otherwise the year of birth of given fan.
 */
int fanGetDateOfBirth(Fan);

/**
 * fanCompareFansByAge: Returns the year of birth of a given fan
 * @param fan - The target fan which year of birth is requested.
 * @return
 * 	-2 if a NULL pointer was sent.
 * 	Otherwise:
 *  1 if first fan is bigger
 *  0 if fans are of the same age
 *  -1 if second fan is bigger
 */
int fanCompareFansByAge(Fan,Fan);

/**
 * fanCompareFansByAge: Returns the year of birth of a given fan
 * @param fan - The target fan which year of birth is requested.
 * @return
 * 	-2 if a NULL pointer was sent.
 * 	Otherwise:
 *  1 if first fan is bigger
 *  0 if fans are of the same age
 *  -1 if second fan is bigger
 */
// i think we dont use it
int fanCompareFansById(Fan,Fan);

#endif
