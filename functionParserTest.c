//
//  leagueTest.c
//  LeagueSvn
//
//  Created by Efi Shtain on 12/14/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "functionParser.h"

#define ASSERT(expr) do { \
if(!(expr)) { \
printf("\nAssertion failed %s (%s:%d).\n", #expr, __FILE__, __LINE__); \
return false; \
} else { \
printf("."); \
} \
} while (0)

#define RUN_TEST(test) do { \
printf("Running "#test); \
if(test()) { \
printf("[OK]\n"); \
} \
} while(0)

#define ASSERT_NULL(expr) ASSERT((expr) == NULL)
#define ASSERT_NOT_NULL(expr) ASSERT((expr) != NULL)
#define ASSERT_EQUAL(expr1,expr2) ASSERT ((expr1)==(expr2))


bool testCreateDestroy(){
    FunctionParser parser = functionParserCreate(NULL);
    ASSERT_NULL(parser);
    League league = leagueCreate();
    parser = functionParserCreate(league);
    ASSERT_NOT_NULL(parser);
    functionParserDestroy(parser);
    leagueDestroy(league);
    return true;
}

bool checkString(FunctionParser parser, const char* string, FunctionParserResult parserResult, LeagueResult leagueResult){
    LeagueResult result=leagueResult;
    char command[120];
    strcpy(command, string);
    ASSERT_EQUAL(functionParserParseStringAndExecute(parser, command, &result),parserResult);
    ASSERT_EQUAL(result, leagueResult);
    return true;
}

bool fakeReportFunction(ReportType type,...){
    return true;
}

bool testParseString(){
    League league = leagueCreate();
    FunctionParser parser = functionParserCreate(league);
    checkString(parser, "", FUNCTION_PARSER_EMPTY_LINE, LEAGUE_SUCCESS);
    checkString(parser, "\n",FUNCTION_PARSER_EMPTY_LINE , LEAGUE_SUCCESS);
    checkString(parser, "#", FUNCTION_PARSER_MARKED_LINE, LEAGUE_SUCCESS);
    checkString(parser, "invalid", FUNCTION_PARSER_INVALID_COMMAND, LEAGUE_SUCCESS);
    checkString(parser, "formation", FUNCTION_PARSER_INVALID_COMMAND, LEAGUE_SUCCESS);
    checkString(parser, "fan", FUNCTION_PARSER_INVALID_COMMAND, LEAGUE_SUCCESS);
    checkString(parser, "report", FUNCTION_PARSER_INVALID_COMMAND, LEAGUE_SUCCESS);
    checkString(parser, "formation add efi", FUNCTION_PARSER_SUCCESS, LEAGUE_SUCCESS);
    checkString(parser, "formation remove efi", FUNCTION_PARSER_SUCCESS, LEAGUE_SUCCESS);
    checkString(parser, "formation remove efi", FUNCTION_PARSER_SUCCESS, LEAGUE_FORMATION_NOT_EXIST);
    checkString(parser, "fan add 123 efi 1986", FUNCTION_PARSER_SUCCESS, LEAGUE_SUCCESS);
    checkString(parser, "fan remove 123", FUNCTION_PARSER_SUCCESS, LEAGUE_SUCCESS);
    checkString(parser, "fan remove 123", FUNCTION_PARSER_SUCCESS, LEAGUE_FAN_DOES_NOT_EXIST);
    checkString(parser, "report fans", FUNCTION_PARSER_SUCCESS, LEAGUE_NO_PRINT_CALLBACK);
    checkString(parser, "report formations 3", FUNCTION_PARSER_SUCCESS, LEAGUE_NO_PRINT_CALLBACK);
    checkString(parser, "report formations_by_age", FUNCTION_PARSER_SUCCESS, LEAGUE_NO_PRINT_CALLBACK);
    leagueSetPrintReportFunction(league, fakeReportFunction);
    checkString(parser, "report fans", FUNCTION_PARSER_SUCCESS, LEAGUE_NO_FANS);
    checkString(parser, "report formations 3", FUNCTION_PARSER_SUCCESS, LEAGUE_NO_FORMATIONS);
    checkString(parser, "report formations_by_age", FUNCTION_PARSER_SUCCESS, LEAGUE_NO_FORMATIONS);
    checkString(parser, "formation add efi", FUNCTION_PARSER_SUCCESS, LEAGUE_SUCCESS);
    checkString(parser, "report formations_by_age", FUNCTION_PARSER_SUCCESS, LEAGUE_NO_FANS);
    checkString(parser, "fan add 123 efi 1986", FUNCTION_PARSER_SUCCESS, LEAGUE_SUCCESS);
    checkString(parser, "report formations_by_age", FUNCTION_PARSER_SUCCESS, LEAGUE_SUCCESS);
    checkString(parser, "formation join efi 123", FUNCTION_PARSER_SUCCESS, LEAGUE_SUCCESS);
    checkString(parser, "formation join efi 123", FUNCTION_PARSER_SUCCESS, LEAGUE_FAN_ALREADY_IN_FORMATION);
    checkString(parser, "formation leave efi 123", FUNCTION_PARSER_SUCCESS, LEAGUE_SUCCESS);
    checkString(parser, "formation leave efi 123", FUNCTION_PARSER_SUCCESS, LEAGUE_FAN_NOT_IN_FORMATION);
    functionParserDestroy(parser);
    leagueDestroy(league);
    
    return true;
}
//int main(int argc, char** argv){
//    RUN_TEST(testCreateDestroy);
//    RUN_TEST(testParseString);
//}









