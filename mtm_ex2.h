#ifndef MTM_EX2_H_
#define MTM_EX2_H_

#include <stdio.h> // For FILE*
				   // You should avoid including unnecessary header files
#include <stdbool.h>

/**
 * Maximal input line length
 */
#define MAX_LEN 120
#define YEAR 2013

/**
 * This type defines all errors for the system.
 * These codes should be used for calling mtmPrintErrorMessage to report
 * error messages.
 *
 * Notice that after calling mtmPrintErrorMessage with one of the first
 * three messages you should abort the program according to the exercise's
 * rules
 */
typedef enum {
	MTM_OUT_OF_MEMORY, // You should exit program after this error
	MTM_INVALID_COMMAND_LINE_PARAMETERS, // You should exit the program after
										 // this error
	MTM_CANNOT_OPEN_FILE, // You should exit program after this error
	MTM_FORMATION_ALREADY_EXISTS,
	MTM_FORMATION_DOES_NOT_EXIST,
	MTM_NO_FORMATIONS,
	MTM_FAN_ALREADY_EXISTS,
	MTM_FAN_DOES_NOT_EXIST,
	MTM_NO_FANS,
	MTM_FAN_ALREADY_IN_FORMATION,
	MTM_FAN_NOT_IN_FORMATION,
	MTM_INVALID_PARAMETERS,
} MtmErrorCode;

/**
 * mtmPrintErrorMessage - prints an error message into the given output channel
 * Use the standard error channel to print error messages.
 *
 * @param errorChannel - File descriptor for the error channel
 * @param code - Error code to print message for.
 */
void mtmPrintErrorMessage(FILE* errorChannel, MtmErrorCode code);

/**
 * Prints the description of the formation.
 * @param output - File descriptor for the output channel
 * @param formationName - The name of the formation.
 * @param formationSize - The number of fans in the formation.
 */
void mtmPrintFormation(FILE* output, const char* formationName, int formationSize);

/**
 * Prints the description of the fan.
 * @param output - File descriptor for the output channel
 * @param fanName - The name of the fan.
 * @param birthyear - The year of birth of the fan.
 * @param id - The fan's ID number.
 * @param formations - The number of formations the fan supports.
 */
void mtmPrintFan(FILE* output, const char* fanName, int birthyear, int id, int formations);

/**
 * Prints the description of the formations for a given birth year.
 * @param output - File descriptor for the output channel
 * @param birthyear - The year of birth of the fan.
 * @param formationName - An array with the names of the formations.
 * @param arrayLength - The number of formations in the array.
 */
void mtmPrintFormationsByAge(FILE* output, int birthyear, const char** formationsNames, int arrayLength);

#endif /* MTM_EX2_H_ */
