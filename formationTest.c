///*
//
// * formation tests.c
// *
// *  Created on: Dec 13, 2013
// *      Author: Yakir
// */
//
//
//#include <stdio.h>
//
//#include <stdlib.h>
//#include <stdbool.h>
//#include <string.h>
//#include "formation.h"
//
//
//#define ASSERT(expr) do { \
//	if(!(expr)) { \
//		printf("\nAssertion failed %s (%s:%d).\n", #expr, __FILE__, __LINE__); \
//		return false; \
//	} else { \
//		printf("."); \
//	} \
//} while (0)
//
//#define RUN_TEST(test) do { \
//  printf("Running "#test); \
//  if(test()) { \
//    printf("[OK]\n"); \
//  } \
//} while(0)
//
//#define ASSERT_NULL_ARGUMENT(expr) ASSERT(expr == NULL)
//#define ASSERT_FORMATION_NULL_ARG(expr) ASSERT(expr==FORMATION_NULL_ARGUMENT);
//#define ASSERT_FORMATION_SUCCESS(expr) 	ASSERT(expr==FORMATION_SUCCESS)
//#define ASSERT_FAN_ALREADY_EXITS(expr) ASSERT(expr==FORMATION_FAN_ALREADY_EXIST)
//#define ASSERT_NOT_NULL(expr) ASSERT((expr) != NULL)
//#define ASSERT_TRUE(expr) ASSERT((expr) == true)
//#define ASSERT_FAN_NOT_EXISTS(expr) ASSERT(expr== FORMATION_FAN_NOT_EXIST)
//#define ASSERT_FALSE(expr) ASSERT((expr) == false)
////#define ASSERT_NO_ERROR(expr) expr
////#define ASSERT_ALREADY_FULL(expr) ASSERT((expr) == FORMATION_ALREADY_FULL)
//#define ASSERT_CORRECT_INT(expr1,expr2) ASSERT (expr1==expr2)
//#define ASSERT_CORRECT_BIRTHE(expr1,expr2) ASSERT (expr1==expr2)
//#define ASSERT_CORRECT_ID(expr1,expr2) ASSERT (expr1==expr2)
//
///*
// * testFormationCreateDestroy : check formation create and destroy
// */
//static bool testFormationCreateDestroy(void) {
//	Formation formation = formationCreate(NULL);
//	ASSERT_NULL_ARGUMENT(formation);
//
//	Formation formation1 = formationCreate("yakir2_!13r");
//	ASSERT_NOT_NULL(formation1);
//	Fan fans[6];
//	fans[0] = fanCreate(5234,"yakir",0);
//	fans[1] = fanCreate(34,"efi",0);
//	fans[2] = fanCreate(1,"eewvv w",0);
//	fans[3] = fanCreate(0,"sdvberb",0);
//	fans[4] = fanCreate(9,"33!@#$1f1",0);
//	fans[5] = fanCreate(575834,"$#34re",0);
//
//	for (int i=0; i<6;i++){
//		formationJoinFan(formation1,fans[i]);
//	}
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation1),6);
//	Fan fan1 = formationGetFirstFan(formation1);
//	char* fan1Name=fanGetName(fan1);
//	char* fan2Name=NULL;
//
//	for (int i=0; i<formationGetNumberOfFans(formation1);i++) {
//		fan2Name = fanGetName(fans[i]);
//		ASSERT_CORRECT_ID(fanGetId(fan1),fanGetId(fans[i]));
//		ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan1),fanGetDateOfBirth(fans[i]));
//		ASSERT_CORRECT_INT(strcmp(fan1Name,fan2Name),0);
//		free(fan1Name);
//		free(fan2Name);
//		fan1 = formationGetNextFan(formation1);
//		fan1Name= fanGetName(fan1);
//	}
//	free(fan1Name);
//	ASSERT_CORRECT_INT(strcmp(formationGetName(formation1),"yakir2_!13r"),0);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation1),6);
//	for(int i=0; i<6 ; i++) {
//		fanDestroy(fans[i]);
//	}
//	formationDestroy(formation1);
//
//
//	Formation formation2 = formationCreate("-1!_efi_!1");
//	ASSERT_NOT_NULL(formation2);
//	ASSERT_CORRECT_INT(strcmp(formationGetName(formation2),"-1!_efi_!1"),0);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation2),0);
//	// check destroy
//	formationDestroy(formation2);
//
//
//	// check if we change string name so formation name will not change
//	char myString[5] = {'w', 'i', 'k', 'i', 0};
//	Formation formation3 = formationCreate(myString);
//	ASSERT_NOT_NULL(formation3);
//	ASSERT_CORRECT_INT(strcmp(formationGetName(formation3),"wiki"),0);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation3),0);
//	myString[0]= 'p';
//	ASSERT_CORRECT_INT(strcmp(myString,"piki"),0);
//	ASSERT_CORRECT_INT(strcmp("wiki",formationGetName(formation3)),0);
//	formationDestroy(formation3);
//
//	return true;
//
//}
//
//
//static bool testFormationCopy(void) {
//	//i didn't understand 2 isntructions
//	Formation formation = formationCreate(NULL);
//	ASSERT_NULL_ARGUMENT(formation);
//	Formation CopyOfFormation = formationCopy(formation);
//	ASSERT_NULL_ARGUMENT(CopyOfFormation);
//
//	Formation formation1 = formationCreate("Menamonic");
//	Fan fans[6];
//	fans[0] = fanCreate(5234,"yakir",0);
//	fans[1] = fanCreate(34,"efi",0);
//	fans[2] = fanCreate(1,"eewvv w",0);
//	fans[3] = fanCreate(0,"sdvberb",0);
//	fans[4] = fanCreate(9,"33!@#$1f1",0);
//	fans[5] = fanCreate(575834,"$#34re",0);
//
//	for (int i=0; i<6;i++){
//		formationJoinFan(formation1,fans[i]);
//	}
//
//	ASSERT_NOT_NULL(formation1);
//	Formation formation1Copy = formationCopy(formation1);
//	ASSERT_NOT_NULL(formation1Copy);
//	// check all values are equal
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation1),
//			formationGetNumberOfFans(formation1Copy));
//	ASSERT_CORRECT_INT(strcmp(formationGetName(formation1),
//			formationGetName(formation1Copy)),0);
//	// check fans are equal
//	Fan fan1 = formationGetFirstFan(formation1);
//	Fan fan2 = formationGetFirstFan(formation1Copy);
//	char* fan1Name=fanGetName(fan1);
//	char* fan2Name=fanGetName(fan2);
//	for (int i=1; i<=formationGetNumberOfFans(formation1);i++){
//		ASSERT_CORRECT_ID(fanGetId(fan1),fanGetId(fan2));
//		ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan1),fanGetDateOfBirth(fan2));
//		ASSERT_CORRECT_INT(strcmp(fan1Name,fan2Name),0);
//		free(fan1Name);
//		free(fan2Name);
//		fan1 = formationGetNextFan(formation1);
//		fan2 = formationGetNextFan(formation1Copy);
//		fan1Name=fanGetName(fan1);
//		fan2Name=fanGetName(fan2);
//	}
//	free(fan1Name);
//	free(fan2Name);
//	fan1= NULL;
//	fan2=NULL;
//	// destroy copy and check the original is ok
//	formationDestroy(formation1Copy);
//	ASSERT_NOT_NULL(formation1);
//	ASSERT_CORRECT_INT(strcmp(formationGetName(formation1),"Menamonic"),0);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation1),6);
//	Fan fan3 = formationGetFirstFan(formation1);
//	char* fan3Name=fanGetName(fan3);
//	char* fan4Name=NULL;
//	for (int i=0; i<formationGetNumberOfFans(formation1);i++) {
//		fan4Name = fanGetName(fans[i]);
//		ASSERT_CORRECT_ID(fanGetId(fan3),fanGetId(fans[i]));
//		ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan3),fanGetDateOfBirth(fans[i]));
//		ASSERT_CORRECT_INT(strcmp(fan3Name,fan4Name),0);
//		free(fan3Name);
//		free(fan4Name);
//		fan3 = formationGetNextFan(formation1);
//		fan3Name=fanGetName(fan3);
//	}
//	free(fan3Name);
//	fan3=NULL;
//
//	for (int i=0; i<6;i++){
//		fanDestroy(fans[i]);
//	}
//	fanDestroy(fan1);
//	fanDestroy(fan2);
//	fanDestroy(fan3);
//	formationDestroy(formation1);
//
//	return true;
//
//}
//
//static bool testFormationJoinFan(void) {
//	//check NULL
//	Fan fan = fanCreate(23,"yakir",2000);
//	ASSERT_NOT_NULL(fan);
//	Formation formation = formationCreate("barca");
//	ASSERT_NOT_NULL(formation);
//	FormationResult result = formationJoinFan(NULL,fan);
//	ASSERT_FORMATION_NULL_ARG(result);
//	FormationResult result1 = formationJoinFan(formation,NULL);
//	ASSERT_FORMATION_NULL_ARG(result1);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),0);
//
//	// add fan and check success and number of fan
//	FormationResult result3 = formationJoinFan(formation,fan);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),1);
//
//	// good value for fan already checked in formation create/destroy
//	ASSERT_FORMATION_SUCCESS(result3);
//	FormationResult result4 = formationJoinFan(formation,fan);
//	ASSERT_FAN_ALREADY_EXITS(result4);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),1);
//
//	// what happend if we destroy fan?
//	//the check of correct fans has been done in formation create
//
//	formationDestroy(formation);
//	fanDestroy(fan);
//
//	return true;
//
//}
//
//static bool testFormationRemoveFan(void){
//	Fan fan = fanCreate(23,"yakir",2000);
//	ASSERT_NOT_NULL(fan);
//	Formation formation = formationCreate("barca");
//	ASSERT_NOT_NULL(formation);
//	FormationResult result = formationJoinFan(NULL,fan);
//	ASSERT_FORMATION_NULL_ARG(result);
//	FormationResult result1 = formationJoinFan(formation,NULL);
//	ASSERT_FORMATION_NULL_ARG(result1);
//	FormationResult result2 = formationJoinFan(formation,fan);
//	ASSERT_FORMATION_SUCCESS(result2);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),1);
//	Fan fan3 = fanCreate(0,"yakir",2000);
//	ASSERT_NOT_NULL(fan3);
//	//try to move fan that is not in formation
//	FormationResult result3 = formationRemoveFan(formation,fan3);
//	ASSERT_FAN_NOT_EXISTS(result3);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),1);
//	//remove valid fan
//	FormationResult result4 = formationJoinFan(formation,fan3);
//	ASSERT_FORMATION_SUCCESS(result4);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),2);
//
//	FormationResult result5 = formationRemoveFan(formation,fan);
//	ASSERT_FORMATION_SUCCESS(result5);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),1);
//	//check that the correct fan removed
//	// mybe list contain to find the fan?
//	Fan fan2 = formationGetFirstFan(formation);
//	ASSERT_CORRECT_ID(fanGetId(fan3),fanGetId(fan2));
//	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan3),fanGetDateOfBirth(fan2));
//	ASSERT_CORRECT_ID(fanGetId(fan3),fanGetId(fan2));
//	FormationResult result6 = formationRemoveFan(formation,fan3);
//	ASSERT_FORMATION_SUCCESS(result6);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),0);
//	fan2=NULL;
//	fanDestroy(fan2);
//	fanDestroy(fan3);
//	fanDestroy(fan);
//	formationDestroy(formation);
//
//	return true;
//}
//
//static bool testformationGetNumberOfFans(void) {
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(NULL),0);
//	Formation formation = formationCreate("yakir2_!13r");
//	ASSERT_NOT_NULL(formation);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),0);
//	Fan fans[6];
//	fans[0] = fanCreate(5234,"yakir",0);
//	fans[1] = fanCreate(34,"efi",0);
//	fans[2] = fanCreate(1,"eewvv w",0);
//	fans[3] = fanCreate(0,"sdvberb",0);
//	fans[4] = fanCreate(9,"33!@#$1f1",0);
//	fans[5] = fanCreate(575834,"$#34re",0);
//
//	for (int i=0; i<6;i++){
//		formationJoinFan(formation,fans[i]);
//	}
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),6);
//	for (int i=0; i<6;i++){
//			formationRemoveFan(formation,fans[i]);
//			ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),5-i);
//		}
//	for (int i=0; i<6;i++){
//		fanDestroy(fans[i]);
//	}
//	formationDestroy(formation);
//
//	return true;
//}
//
//static bool testFormationGetName(void){
//	ASSERT_NULL_ARGUMENT(formationGetName(NULL));
//	// we are getting the actual name!!
//	Formation formation = formationCreate("Madrid1!3__+");
//	ASSERT_NOT_NULL(formation);
//	char* formationName = formationGetName(formation);
//	ASSERT_CORRECT_INT(strcmp(formationName,"Madrid1!3__+"),0);
//	// change the value of the returned copy - verify the formationís name has changed
//	formationName[0]='p';
//	ASSERT_CORRECT_INT(strcmp(formationName,"padrid1!3__+"),0);
//	ASSERT_CORRECT_INT(strcmp(formationGetName(formation),"padrid1!3__+"),0);
//	//check if we change a string so formation name dont change
//	//made in destroy/create
//	formationName=NULL;
//	formationDestroy(formation);
//	free(formationName);
//
//	return true;
//
//}
//
//static bool testFormationHasMoreFans(void){
//	Formation formation1 = formationCreate("Madrid1!3__+");
//	ASSERT_NOT_NULL(formation1);
//	Formation formation2 = formationCreate("--!!barca!!--");
//	ASSERT_NOT_NULL(formation2);
//	ASSERT_CORRECT_INT(formationHasMoreFans(NULL,formation2),-2);
//	ASSERT_CORRECT_INT(formationHasMoreFans(formation1,NULL),-2);
//
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation1),0);
//	Fan fans[8];
//	fans[0] = fanCreate(525344,"ywtrsdr",23);
//	fans[1] = fanCreate(34,"weffewi",356);
//	fans[2] = fanCreate(25,"efsvw",234);
//	fans[3] = fanCreate(0,"sdvberb",222);
//	fans[4] = fanCreate(9,"33!@#$1f1",34);
//	fans[5] = fanCreate(575834,"$#34re",5);
//	fans[6] = fanCreate(44,"33!@#$1f1",34);
//	fans[7] = fanCreate(4544,"$#34re",5);
//
//	for (int i=0; i<8;i++){
//		FormationResult result[8];
//		result[i] = formationJoinFan(formation1,fans[i]);
//		ASSERT_FORMATION_SUCCESS(result[i]);
//	}
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation1),8);
//
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation2),0);
//	Fan fans2[7];
//	fans2[0] = fanCreate(525344,"ywtrsdr",23);
//	fans2[1] = fanCreate(34,"weffewi",356);
//	fans2[2] = fanCreate(25,"efsvw",234);
//	fans2[3] = fanCreate(0,"sdvberb",222);
//	fans2[4] = fanCreate(9,"33!@#$1f1",34);
//	fans2[5] = fanCreate(575834,"$#34re",5);
//	fans2[6] = fanCreate(44,"33!@#$1f1",34);
//
//
//	for (int i=0; i<7;i++){
//		FormationResult result1[7];
//		result1[i] = formationJoinFan(formation2,fans2[i]);
//		ASSERT_FORMATION_SUCCESS(result1[i]);
//	}
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation2),7);
//
//// formation 1 has 8 fans and formation 2 has 7 fans
//	ASSERT_CORRECT_INT(formationHasMoreFans(formation2,formation1),1);
//	ASSERT_CORRECT_INT(formationHasMoreFans(formation1,formation2),-1);
//	//remove one fan from formation 1 and check they have the same
//	//number of fans
//	ASSERT_FORMATION_SUCCESS(formationRemoveFan(formation1,fans[7]));
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation1),7);
//	ASSERT_CORRECT_INT(formationHasMoreFans(formation1,formation2),0);
//
//	for (int i=0; i<8;i++){
//		fanDestroy(fans[i]);
//	}
//	//if we do fan destroy so formation steel have 7 fans why?
//	//ASSERT_CORRECT_INT(formationGetNumberOfFans(formation1),7);
//	for (int i=0; i<7;i++){
//		fanDestroy(fans2[i]);
//	}
//	formationDestroy(formation1);
//	formationDestroy(formation2);
//
//	return true;
//}
//
//static bool testFormationGetFristGetNextFan(void){
//	ASSERT_NULL_ARGUMENT(formationGetFirstFan(NULL));
//	ASSERT_NULL_ARGUMENT(formationGetNextFan(NULL));
//	Formation formation = formationCreate("yakir2_!13r");
//	ASSERT_NOT_NULL(formation);
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),0);
//	Fan fans[6];
//	fans[0] = fanCreate(5234,"yakir",0);
//	fans[1] = fanCreate(34,"efi",0);
//	fans[2] = fanCreate(1,"eewvv w",0);
//	fans[3] = fanCreate(0,"sdvberb",0);
//	fans[4] = fanCreate(9,"33!@#$1f1",0);
//	fans[5] = fanCreate(575834,"$#34re",0);
//
//	for (int i=0; i<6;i++){
//		formationJoinFan(formation,fans[i]);
//	}
//	ASSERT_CORRECT_INT(formationGetNumberOfFans(formation),6);
//	Fan fan = formationGetFirstFan(formation);
//	char* fan1Name=fanGetName(fan);
//	char* fan2Name=fanGetName(fans[0]);
//	ASSERT_CORRECT_INT(strcmp(fan1Name,fan2Name),0);
//	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan),fanGetDateOfBirth(fans[0]));
//	ASSERT_CORRECT_ID(fanGetId(fan),fanGetId(fans[0]));
//
//	fanDestroy(fans[0]);
//	ASSERT_CORRECT_INT(strcmp(fan1Name,"yakir"),0);
//	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan),0);
//	ASSERT_FORMATION_SUCCESS(formationRemoveFan(formation,fan));
//
//	fan = formationGetFirstFan(formation);
//	long id= fanGetId(fan);
//	ASSERT_TRUE(id==34);
//	ASSERT_FALSE(id==5234);
//	fan = formationGetNextFan(formation);
//	free(fan2Name);
//	free(fan1Name);
//
//	fan1Name=fanGetName(fan);
//	fan2Name=fanGetName(fans[2]);
//	ASSERT_CORRECT_INT(strcmp(fan1Name,fan2Name),0);
//	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan),fanGetDateOfBirth(fans[2]));
//	ASSERT_CORRECT_ID(fanGetId(fan),fanGetId(fans[2]));
//	int i=3;
//	while((fan = formationGetNextFan(formation))){
//	 ASSERT_TRUE(	fanCompareFansById(fan,fans[i])==0);
//	 i++;
//	}
//	formationDestroy(formation);
//	for (int i=1; i<6;i++){
//		fanDestroy(fans[i]);
//	}
//	free(fan2Name);
//	free(fan1Name);
//	return true;
//}
//
//
//int main(){
//	RUN_TEST(testFormationCreateDestroy);
//	RUN_TEST(testFormationCopy);
//	RUN_TEST(testFormationJoinFan);
//	RUN_TEST(testFormationRemoveFan);
//	RUN_TEST(testformationGetNumberOfFans);
//	RUN_TEST(testFormationGetName);
//	RUN_TEST(testFormationHasMoreFans);
//	RUN_TEST(testFormationGetFristGetNextFan);
//	return 0;
//}
//
