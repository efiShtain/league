/*

 * map tests.c
 *
 *  Created on: Dec 13, 2013
 *      Author: Yakir
 */


#include <stdio.h>

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "map.h"

MapDataElement copyData(MapDataElement data){
	if (data==NULL){
		return NULL;
	}
	int* dataCopy =malloc(sizeof(*dataCopy));
	*dataCopy = *((int*)data);
	return (MapDataElement)dataCopy;
}

void freeData(MapDataElement data) {
	free(data);
}

MapDataElement copyKey(MapKeyElement key){
	if (key==NULL){
		return NULL;
	}
	int* keyCopy =malloc(sizeof(*keyCopy));
	*(keyCopy) = *(int*)key;
	return (MapDataElement)keyCopy;
}

void freeKey(MapKeyElement key) {
	free(key);
}

int compareKey(MapKeyElement key1, MapKeyElement key2){

	if (*(int*)key1 == *(int*)key2) {
		return 0;
	}
	if (*(int*)key1 > *(int*)key2) {
		return 1;
	}
		return -1;
}



#define ASSERT(expr) do { \
	if(!(expr)) { \
		printf("\nAssertion failed %s (%s:%d).\n", #expr, __FILE__, __LINE__); \
		return false; \
	} else { \
		printf("."); \
	} \
} while (0)

#define RUN_TEST(test) do { \
  printf("Running "#test); \
  if(test()) { \
    printf("[OK]\n"); \
  } \
} while(0)

#define ASSERT_NULL_ARGUMENT(expr) ASSERT(expr == NULL)
#define ASSERT_MAP_NULL_ARGUMENT(expr) ASSERT(expr == MAP_NULL_ARGUMENT)
#define ASSERT_MAP_SUCCESS(expr) ASSERT (expr == MAP_SUCCESS)
#define ASSERT_MAP_SUCCESS_ITEM_ADDED(expr) ASSERT(expr==MAP_SUCCESS_ITEM_ADDED)
#define ASSERT_MAP_ITEM_DOES_NOT_EXIST(expr) ASSERT(expr==MAP_ITEM_DOES_NOT_EXIST)
//#define ASSERT_FORMATION_NULL_ARG(expr) ASSERT(expr==FORMATION_NULL_ARGUMENT);
//#define ASSERT_FORMATION_SUCCESS(expr) 	ASSERT(expr==FORMATION_SUCCESS)
//#define ASSERT_FAN_ALREADY_EXITS(expr) ASSERT(expr==FORMATION_FAN_ALREADY_EXIST)
#define ASSERT_NOT_NULL(expr) ASSERT((expr) != NULL)
#define ASSERT_TRUE(expr) ASSERT((expr) == true)
//#define ASSERT_FAN_NOT_EXISTS(expr) ASSERT(expr== FORMATION_FAN_NOT_EXIST)
#define ASSERT_FALSE(expr) ASSERT((expr) == false)
//#define ASSERT_NO_ERROR(expr) expr
//#define ASSERT_ALREADY_FULL(expr) ASSERT((expr) == FORMATION_ALREADY_FULL)
#define ASSERT_CORRECT_INT(expr1,expr2) ASSERT (expr1==expr2)
//#define ASSERT_CORRECT_BIRTHE(expr1,expr2) ASSERT (expr1==expr2)
//#define ASSERT_CORRECT_ID(expr1,expr2) ASSERT (expr1==expr2)

static bool testMapCreateDestroy(void){
	ASSERT_NULL_ARGUMENT(mapCreate(NULL,copyKey,
		freeData,freeKey, compareKey));

	ASSERT_NULL_ARGUMENT(mapCreate(copyData,NULL,
		freeData,freeKey, compareKey));

	ASSERT_NULL_ARGUMENT(mapCreate(copyData,copyKey,
			NULL,freeKey, compareKey));

	ASSERT_NULL_ARGUMENT(mapCreate(copyData,copyKey,
		freeData,NULL, compareKey));

	ASSERT_NULL_ARGUMENT(mapCreate(copyData,copyKey,
		freeData,freeKey, NULL));

	Map map = mapCreate(copyData,copyKey,
		freeData,freeKey,compareKey);
	ASSERT_NOT_NULL(map);

	//check if there aren't memory leaks
	mapDestroy(map);
	return true;
}

static bool testMapPutMapGetMapCopy(void) {
	Map map = mapCreate(copyData,copyKey,freeData,freeKey,compareKey);
	ASSERT_NOT_NULL(map);

	int* data = malloc(sizeof(*data));
	int* key = malloc(sizeof(*key));
	*key= 1;
	*data= 1;
	ASSERT_MAP_NULL_ARGUMENT(mapPut(NULL,key,data));
	ASSERT_MAP_SUCCESS_ITEM_ADDED(mapPut(map,key,data));
	*data = 2;
	ASSERT_MAP_SUCCESS(mapPut(map,key,data));
	*data=1;
	ASSERT_MAP_SUCCESS(mapPut(map,key,data));
	for (int i=2;i<=100;i++){
		*key=i;
		*data=i;
		ASSERT_MAP_SUCCESS_ITEM_ADDED(mapPut(map,key,data));
	}
	int* data1=NULL;
	for (int i=1; i<=100; i++){
		data1 =(int*)(mapGet(map,&i));
		ASSERT_CORRECT_INT(*data1,i);
		free(data1);
	}
	int* data2=malloc(sizeof(*data2));
	for (int i=1;i<=100;i++){
		*key=i;
		*data2=i+1;
		ASSERT_MAP_SUCCESS(mapPut(map,key,data2));
	}
	free(data2);
	int* data3=NULL;
	for (int i=1; i<=100; i++){
		data3 =(int*)(mapGet(map,&i));
		ASSERT_CORRECT_INT(*data3,i+1);
		free(data3);
		data3 =(int*)(mapGet(map,&i));
		ASSERT_CORRECT_INT(*data3,i+1);
		free(data3);
	}
	ASSERT_NULL_ARGUMENT(mapCopy(NULL));
	Map map2 = mapCopy(map);
	ASSERT_NOT_NULL(map2);
	ASSERT_CORRECT_INT(mapGetSize(NULL),-1);
	ASSERT_CORRECT_INT(mapGetSize(map),100);
	ASSERT_CORRECT_INT(mapGetSize(map2),100);
	int* data4= NULL;
	int* data5 = NULL;
	for (int i=1;i<=100;i++) {
		data4 =(int*)(mapGet(map,&i));
		data5 =(int*)(mapGet(map2,&i));
		ASSERT_CORRECT_INT(*data4,*data5);
		free(data4);
		free(data5);
	}
	mapDestroy(map);
	mapDestroy(map2);
	Map map3 = mapCreate(copyData,copyKey,freeData,freeKey,compareKey);
	int numbersArray2[10]={2,1,4,3,5,6,9,8,7,10};
	for(int i=1;i<=10;i++){
		*key=i ;
		*data=numbersArray2[i-1];
		ASSERT_MAP_SUCCESS_ITEM_ADDED(mapPut(map3,key,data));
	}
	int* data9=NULL;
	for (int i=1; i<=10; i++){
		data9 =(int*)(mapGet(map3,&i));
		ASSERT_CORRECT_INT(*data9,numbersArray2[i-1]);
		free(data9);
	}
	MapResult result = mapClear(NULL);
	ASSERT_MAP_NULL_ARGUMENT(result);
	result = mapClear(map3);
	int numberr=1;
	for (int i=1;i<11;i++) {
		ASSERT_FALSE(mapContains(map3,&numberr));
		numberr++;
	}
	ASSERT_CORRECT_INT(mapGetSize(map3),0);
	mapDestroy(map3);
	free(data);
	free(key);

	/*MapResult result;
	ASSERT_CORRECT_INT(*((int*)mapGetFirst(map3,&result)),numbersArray2[1]);
	ASSERT_CORRECT_INT(*((int*)mapGetNext(map3,&result)),numbersArray2[0]);
	ASSERT_CORRECT_INT(*((int*)mapGetNext(map3,&result)),numbersArray2[3]);
	ASSERT_CORRECT_INT(*((int*)mapGetNext(map3,&result)),numbersArray2[2]);
	ASSERT_CORRECT_INT(*((int*)mapGetNext(map3,&result)),numbersArray2[4]);
	ASSERT_CORRECT_INT(*((int*)mapGetNext(map3,&result)),numbersArray2[5]);
	ASSERT_CORRECT_INT(*((int*)mapGetNext(map3,&result)),numbersArray2[8]);
	ASSERT_CORRECT_INT(*((int*)mapGetNext(map3,&result)),numbersArray2[7]);
	ASSERT_CORRECT_INT(*((int*)mapGetNext(map3,&result)),numbersArray2[6]);
	ASSERT_CORRECT_INT(*((int*)mapGetNext(map3,&result)),numbersArray2[9]);
	*/
	return true;
}

static bool testMapContainsMapRemoveMapGetSize(void) {
	Map map = mapCreate(copyData,copyKey,freeData,freeKey,compareKey);
	ASSERT_NOT_NULL(map);
	int* data = malloc(sizeof(*data));
	int* key = malloc(sizeof(*key));
	for (int i=1;i<=100;i++){
		*key=i;
		*data=i;
		ASSERT_MAP_SUCCESS_ITEM_ADDED(mapPut(map,key,data));
	}
	ASSERT_CORRECT_INT(mapGetSize(map),100);
	int number = 101;
	ASSERT_FALSE(mapContains(NULL,&number));
	ASSERT_FALSE(mapContains(map,NULL));
	ASSERT_FALSE(mapContains(map,&number));
	for (int i=0;i<101;i++) {
		number++;
		ASSERT_FALSE(mapContains(map,&number));
	}
	number=1;
	for (int i=1;i<101;i++) {
		ASSERT_TRUE(mapContains(map,&number));
		number++;
	}
	number = 0;
	ASSERT_FALSE(mapContains(map,&number));
	number = -1;
	ASSERT_FALSE(mapContains(map,&number));

	MapResult result =mapRemove(NULL,&number);
	ASSERT_MAP_NULL_ARGUMENT(result);
	result =mapRemove(map,NULL);
	ASSERT_MAP_NULL_ARGUMENT(result);
	result= mapRemove(map,&number);
	ASSERT_MAP_ITEM_DOES_NOT_EXIST(result);

	number=1;
	for (int i=1;i<101;i++) {
		ASSERT_CORRECT_INT(mapGetSize(map),101-i);
		result= mapRemove(map,&number);
		ASSERT_MAP_SUCCESS(result);
		ASSERT_FALSE(mapContains(map,&number));
		number++;
	}
	number=0;
	result=mapRemove(map,&number);
	ASSERT_MAP_ITEM_DOES_NOT_EXIST(result);
	ASSERT_CORRECT_INT(mapGetSize(map),0);
	number=-20;
	for (int i=1;i<201;i++) {
		ASSERT_CORRECT_INT(mapGetSize(map),0);
		result= mapRemove(map,&number);
		ASSERT_MAP_ITEM_DOES_NOT_EXIST(result);
		number++;
	}
	free(data);
	free(key);
	mapDestroy(map);

	return true;

}
int main(){

	RUN_TEST(testMapCreateDestroy);
	RUN_TEST(testMapPutMapGetMapCopy);
	RUN_TEST(testMapContainsMapRemoveMapGetSize);
	return 0;
}
