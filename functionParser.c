//
//  functionParser.c
//  League
//
//  Created by Efi Shtain on 11/14/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "functionParser.h"


#define COMMAND_ADD "add"
#define COMMAND_REMOVE "remove"
#define COMMAND_REPORT_FORMATIONS "formations"
#define COMMAND_REPORT_FANS "fans"
#define COMMAND_JOIN "join"
#define COMMAND_LEAVE "leave"
#define COMMAND_REPORT_FORMATION_BY_AGE "formations_by_age"
#define COMMAND_OBJECT_ARGUEMNT 0
#define COMMAND_COMMAND_ARGUMENT 1

struct functionParser_t{
    League league;
};

typedef enum {
    FORMATION_OBJECT=0,
    REPORT_OBJECT=1,
    FAN_OBJECT=2,
    INVALID_OBJECT=-1
}CommandObject;


FunctionParser functionParserCreate(League league){
    
    if(league==NULL){
        return NULL;
    }
    FunctionParser newParser = malloc(sizeof(*newParser));
    if (newParser==NULL) {
        return NULL;
    }
    newParser->league=league;
    return newParser;
}

void functionParserDestroy(FunctionParser parser){
    if(parser!=NULL){
        free(parser);
    }
}


CommandObject stringToCommandObjectTypeConverter(char* string){
    if(strcmp(string, "formation")==0){
        return FORMATION_OBJECT;
    }
    if(strcmp(string, "fan")==0){
        return FAN_OBJECT;
    }
    if(strcmp(string, "report")==0){
        return REPORT_OBJECT;
    }
    return INVALID_OBJECT;
}

char** parseCommand(char* command, int* numberOfArgumentsInCommand,FunctionParserResult* result){
    char **commandArguments =malloc(MAX_ARGUMENTS_PER_COMMAND*sizeof(char*));
    if(commandArguments==NULL){
        *result=FUNCTION_PARSER_MEMEORY_ERROR;
        return NULL;
    }
    commandArguments[COMMAND_OBJECT_ARGUEMNT] = strtok(command,STRTOK_DELIMITERS);
    
    if(commandArguments[COMMAND_OBJECT_ARGUEMNT]==NULL){
        free(commandArguments);
        *result=FUNCTION_PARSER_INVALID_COMMAND;
        return NULL;
    }
    
    (*numberOfArgumentsInCommand)++;
    commandArguments[COMMAND_COMMAND_ARGUMENT]=strtok(NULL, STRTOK_DELIMITERS);
    if(commandArguments[COMMAND_COMMAND_ARGUMENT]==NULL){
        free(commandArguments);
        *result=FUNCTION_PARSER_INVALID_COMMAND;
        return NULL;
    }
    
    (*numberOfArgumentsInCommand)++;
    for(int i=2;i<MAX_ARGUMENTS_PER_COMMAND;i++){
        commandArguments[i]=strtok(NULL,STRTOK_DELIMITERS);
        if(commandArguments[i]!=NULL){
            (*numberOfArgumentsInCommand)++;
        }
    }
    
    return commandArguments;
}


FunctionParserResult executeLeagueFormationCommand(League league, char** parsedCommand, int numberOfArguments, LeagueResult *result){
    char* command = parsedCommand[1];
    char* formationName = parsedCommand[2];
    
    if(strcmp(command, COMMAND_ADD)==0 && numberOfArguments>=3){
        *result=leagueAddFormation(league, formationName);
        if(*result==LEAGUE_NULL_ARGUMENT||*result==LEAGUE_MEMEORY_ERROR){
            return FUNCTION_PARSER_FAILED_TO_EXECUTE;
        }
        return FUNCTION_PARSER_SUCCESS;
    }
    
    if(strcmp(command,COMMAND_REMOVE)==0&& numberOfArguments>=3){
        *result = leagueRemoveFormation(league, formationName);
        if(*result==LEAGUE_NULL_ARGUMENT||*result==LEAGUE_MEMEORY_ERROR){
            return FUNCTION_PARSER_FAILED_TO_EXECUTE;
        }
        return FUNCTION_PARSER_SUCCESS;
    }
    if(numberOfArguments>=3){
        long fanId = atol(parsedCommand[3]);
        if(strcmp(command,COMMAND_JOIN)==0){
            *result = leagueAddFanToFormation(league, formationName, fanId);
            if(*result==LEAGUE_NULL_ARGUMENT||*result==LEAGUE_MEMEORY_ERROR){
                return FUNCTION_PARSER_FAILED_TO_EXECUTE;
            }
            return FUNCTION_PARSER_SUCCESS;
        }
        
        if(strcmp(command,COMMAND_LEAVE)==0){
            *result = leagueRemoveFanFromFormation(league, formationName, fanId);
            if(*result==LEAGUE_NULL_ARGUMENT||*result==LEAGUE_MEMEORY_ERROR){
                return FUNCTION_PARSER_FAILED_TO_EXECUTE;
            }
            return FUNCTION_PARSER_SUCCESS;
        }
    }
    return FUNCTION_PARSER_INVALID_COMMAND;
}

FunctionParserResult executeLeagueFanCommand(League league, char** parsedCommand,int numberOfArguments, LeagueResult *result){
    char* command = parsedCommand[1];
    long fanId = atol(parsedCommand[2]);
    
    if(strcmp(command, COMMAND_REMOVE)==0 && numberOfArguments>=3){
        *result = leagueRemoveFan(league, fanId);
        if(*result==LEAGUE_NULL_ARGUMENT||*result==LEAGUE_MEMEORY_ERROR){
            return FUNCTION_PARSER_FAILED_TO_EXECUTE;
        }
        return FUNCTION_PARSER_SUCCESS;
    }
    
    if(strcmp(command, COMMAND_ADD)==0 && numberOfArguments>=5){
        char* fanName = parsedCommand[3];
        int fanYearOfBirth = atoi(parsedCommand[4]);
        *result = leagueAddFan(league, fanId, fanName, fanYearOfBirth);
        if(*result==LEAGUE_NULL_ARGUMENT||*result==LEAGUE_MEMEORY_ERROR){
            return FUNCTION_PARSER_FAILED_TO_EXECUTE;
        }
        return FUNCTION_PARSER_SUCCESS;
    }
    return FUNCTION_PARSER_INVALID_COMMAND;
    
}

FunctionParserResult executeLeagueReportCommand(League league, char** parsedCommand,int numberOfArguments, LeagueResult *result){
    char* command = parsedCommand[1];
    if(strcmp(command, COMMAND_REPORT_FANS)==0){
        *result = leagueReportFans(league);
        if(*result==LEAGUE_NULL_ARGUMENT||*result==LEAGUE_MEMEORY_ERROR){
            
            return FUNCTION_PARSER_FAILED_TO_EXECUTE;
        }
        return FUNCTION_PARSER_SUCCESS;
    }
    if(strcmp(command, COMMAND_REPORT_FORMATION_BY_AGE)==0){
        *result = leagueReportFormationsByAge(league);
        if(*result==LEAGUE_NULL_ARGUMENT||*result==LEAGUE_MEMEORY_ERROR){
            
            return FUNCTION_PARSER_FAILED_TO_EXECUTE;
        }
        return FUNCTION_PARSER_SUCCESS;
    }
    if(strcmp(command, COMMAND_REPORT_FORMATIONS)==0&&numberOfArguments>=3){
        int formationsCount=0;
        formationsCount= atoi(parsedCommand[2]);
        *result =leagueReportFormations(league, formationsCount);
        if(*result==LEAGUE_NULL_ARGUMENT||*result==LEAGUE_MEMEORY_ERROR){
            return FUNCTION_PARSER_FAILED_TO_EXECUTE;
        }
        return FUNCTION_PARSER_SUCCESS;
    }
    return FUNCTION_PARSER_INVALID_COMMAND;
}

FunctionParserResult executeCommand(FunctionParser parser, char** parsedCommand, int numberOfArgumentsInFunction, LeagueResult *result){
    CommandObject commandObject = stringToCommandObjectTypeConverter(parsedCommand[0]);
    switch (commandObject) {
        case FORMATION_OBJECT:
            return executeLeagueFormationCommand(parser->league,parsedCommand,numberOfArgumentsInFunction,result);
        case REPORT_OBJECT:
            return executeLeagueReportCommand(parser->league,parsedCommand,numberOfArgumentsInFunction,result);
        case FAN_OBJECT:
            return executeLeagueFanCommand(parser->league,parsedCommand,numberOfArgumentsInFunction,result);
        default:
            return FUNCTION_PARSER_INVALID_COMMAND;
    }
    
}

//FunctionParserResult checkForMarkOrEmptyLine(char** parsedCommand,int numberOfArgumentsInCommand){
//    if(parsedCommand==NULL || numberOfArgumentsInCommand==0){
//        return FUNCTION_PARSER_EMPTY_LINE;
//    }
//
//    char firstCharInCommand=parsedCommand[0][0];
//    if(firstCharInCommand==MARKED_LINE_IDENTIFIER){
//        return FUNCTION_PARSER_MARKED_LINE;
//    }
//
//    return FUNCTION_PARSER_VALID_COMMAND;
//}

FunctionParserResult checkForMarkOrEmptyLine(char* command){
    if (command==NULL ||strlen(command)==0) {
        return FUNCTION_PARSER_EMPTY_LINE;
    }
    
    char firstCharInCommand=command[0];
    if(firstCharInCommand=='\n'){
        return FUNCTION_PARSER_EMPTY_LINE;
    }
    if(firstCharInCommand==MARKED_LINE_IDENTIFIER){
        return FUNCTION_PARSER_MARKED_LINE;
    }
    
    return FUNCTION_PARSER_VALID_COMMAND;
}



FunctionParserResult startExecuteCommandProcess(FunctionParser parser, char* command, LeagueResult* result){
    
    int numberOfArgumentsInCommand=0;
    FunctionParserResult parserResult=FUNCTION_PARSER_SUCCESS;
    char** parsedCommandArguments = parseCommand(command,&numberOfArgumentsInCommand,&parserResult);
    if(parserResult!=FUNCTION_PARSER_SUCCESS){
        return parserResult;
    }
    parserResult = executeCommand(parser, parsedCommandArguments,numberOfArgumentsInCommand,result);
    
    free(parsedCommandArguments);
    parsedCommandArguments=NULL;
    return parserResult;
}



FunctionParserResult functionParserParseStringAndExecute(FunctionParser parser,char* command, LeagueResult *result){
    if(parser==NULL|| command==NULL || result==NULL){
        return FUNCTION_PARSER_NULL_ARGUMENT;
    }
    FunctionParserResult checkCommand = checkForMarkOrEmptyLine(command);
    if(checkCommand!=FUNCTION_PARSER_VALID_COMMAND){
        return checkCommand;
    }
    return startExecuteCommandProcess(parser,command, result);
    
}







