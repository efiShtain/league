//
//  formation.c
//  Ex2Mtm
//
//  Created by Efi Shtain on 11/13/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "formation.h"
#include "list.h"


struct formation_t{
    char* formationsName;
    List fans;
    int numberOfFans;
};

static ListElement listFanCopy(ListElement fan){
    return (ListElement)fanCopy((Fan)fan);
    //return fan;
}

static void listFanDestroy(ListElement fan){
    fanDestroy((Fan)fan);
}

FormationResult listResultToFormationResultConverter(ListResult result){
    switch (result) {
        case LIST_NULL_ARGUMENT:
            return FORMATION_NULL_ARGUMENT;
        case LIST_OUT_OF_MEMORY:
            return FORMATION_OUT_OF_MEMEORY;
        case LIST_NO_SUCH_ELEMENT:
            return FORMATION_FAN_NOT_EXIST;
            //case LIST_INVALID_CURRENT:
            
        default:
            return FORMATION_SUCCESS;
    }
}

Formation formationCreate(char* name){
    if(name==NULL){
        return NULL;
    }
    Formation newFormation = malloc(sizeof(*newFormation));
    if(newFormation==NULL){
        return NULL;
    }
    newFormation->formationsName = malloc(strlen(name)+1);
    if (newFormation->formationsName==NULL) {
        formationDestroy(newFormation);
        return NULL;
    }
    strcpy(newFormation->formationsName,name);
    
    newFormation->fans=listCreate(listFanCopy,listFanDestroy);
    if(newFormation->fans==NULL){
        formationDestroy(newFormation);
        return NULL;
    }
    newFormation->numberOfFans=0;
    return newFormation;
}

void formationDestroy(Formation formation){
    if(formation!=NULL){
        free(formation->formationsName);
        formation->formationsName=NULL;
        listDestroy(formation->fans);
        formation->fans=NULL;
        free(formation);
    }
}

Formation formationCopy(Formation formation){
	//yakir added it
	if (formation==NULL){
		return NULL;
	}//
    Formation newFormation = formationCreate(formation->formationsName);
    if (newFormation==NULL) {
        return NULL;
    }
    listDestroy(newFormation->fans);
    newFormation->fans=listCopy(formation->fans);
    if(newFormation->fans==NULL){
        formationDestroy(newFormation);
        return NULL;
    }
    newFormation->numberOfFans=formation->numberOfFans;
    return newFormation;
}

//set the list iterator to the fan, if it is found
static bool formationIsFanInFormation(Formation formation, Fan fan){
    if(formation==NULL || formation->numberOfFans==0 || fan==NULL){
        return false;
    }
    //Might have some memory issues, depends if getFirst and getNext of list returns a copy
    LIST_FOREACH(Fan, ptr, formation->fans){
        if(fanGetId(ptr)==fanGetId(fan)){
            return true;
        }
    }
    return false;
}

FormationResult formationJoinFan(Formation formation, Fan fan){
    
    if(formation==NULL||fan==NULL){
        return FORMATION_NULL_ARGUMENT;
    }
    
    if(formationIsFanInFormation(formation, fan)){
        return FORMATION_FAN_ALREADY_EXIST;
    }
    
    
    ListResult result= listInsertLast(formation->fans,fan);
    
    if(result!=LIST_SUCCESS){
        
        return listResultToFormationResultConverter(result);
    }
    formation->numberOfFans++;
    
    return FORMATION_SUCCESS;
    
}

FormationResult formationRemoveFan(Formation formation, Fan fan){
    if(formation==NULL||fan==NULL){
        return FORMATION_NULL_ARGUMENT;
    }
    if(!formationIsFanInFormation(formation, fan)){
        return FORMATION_FAN_NOT_EXIST;
    }
    
    //pointing on the current if isFanInFormation returned true, using the inner iterator
    ListResult result = listRemoveCurrent(formation->fans);
    if(result!=LIST_SUCCESS){
        return listResultToFormationResultConverter(result);
    }
    formation->numberOfFans--;
    return FORMATION_SUCCESS;
}


int formationGetNumberOfFans(Formation formation){
    if (formation==NULL) {
        return 0;
    }
    return formation->numberOfFans;
}

char* formationGetName(Formation formation){
    if(formation==NULL){
        return NULL;
    }
    return formation->formationsName;
    //    char* nameCopy = malloc(strlen(formation->formationsName)+1);
    //    if(nameCopy==NULL){
    //        return NULL;
    //    }
    //    return strcpy(nameCopy, formation->formationsName);
}

int formationHasMoreFans(Formation formation1,Formation formation2){
    if(formation1==NULL||formation2==NULL){
        return -2;
    }
    if(formation1->numberOfFans>formation2->numberOfFans){
        return -1;
    }
    if(formation1->numberOfFans<formation2->numberOfFans){
        return 1;
    }
    return 0;
}

Fan formationGetFirstFan(Formation formation){
    if(formation==NULL||formation->numberOfFans==0){
        return NULL;
    }
    return listGetFirst(formation->fans);
}

Fan formationGetNextFan(Formation formation){
    if(formation==NULL||formation->numberOfFans==0){
        return NULL;
    }
    return listGetNext(formation->fans);
}
