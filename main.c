#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "functionParser.h"
#include "mtm_ex2.h"

#define OPEN_FILE_FOR_READ "r"
#define OPEN_FILE_FOR_WRITE "w"
#define INPUT_FILE_MARKER "-i"
#define OUTPUT_FILE_MARKER "-o"


static bool setInputOutputSource(const char* filePath, const char* fileOperation, FILE* destination){
    
    FILE* file = freopen(filePath, fileOperation, destination);
    if(file==NULL){
        mtmPrintErrorMessage(stderr, MTM_CANNOT_OPEN_FILE);
        return false;
    }
    return true;
}

static bool checkArguments(int argc, const char * arguments[]){
    switch (argc) {
        case 1:
            return true;
            break;
        case 3:
            if(strcmp(arguments[1], INPUT_FILE_MARKER)==0){
                return setInputOutputSource(arguments[2], OPEN_FILE_FOR_READ,stdin);
            }
            if(strcmp(arguments[1], OUTPUT_FILE_MARKER)==0){
                return setInputOutputSource(arguments[2], OPEN_FILE_FOR_WRITE,stdout);
            }
            mtmPrintErrorMessage(stderr, MTM_INVALID_COMMAND_LINE_PARAMETERS);
            return false;
        case 5:
            if(strcmp(arguments[1],INPUT_FILE_MARKER)==0 && strcmp(arguments[3],OUTPUT_FILE_MARKER)==0){
                bool result= setInputOutputSource(arguments[2], OPEN_FILE_FOR_READ, stdin);
                if(result){
                    return setInputOutputSource(arguments[4], OPEN_FILE_FOR_WRITE, stdout);
                }
                return false;
            }
            if(strcmp(arguments[1],OUTPUT_FILE_MARKER)==0 && strcmp(arguments[3],INPUT_FILE_MARKER)==0){
                bool result = setInputOutputSource(arguments[2], OPEN_FILE_FOR_READ, stdout);
                if(result){
                    return setInputOutputSource(arguments[4], OPEN_FILE_FOR_WRITE, stdin);
                }
                return false;
            }
            
        default:
            mtmPrintErrorMessage(stderr, MTM_INVALID_COMMAND_LINE_PARAMETERS);
            return false;
    }
    
}


static MtmErrorCode leagueResultToMtmErrorCodeConverter(LeagueResult result){
    switch (result) {
        case LEAGUE_FORMATION_ALREADY_EXIST:
            return MTM_FORMATION_ALREADY_EXISTS;
            
        case LEAGUE_FORMATION_NOT_EXIST:
            return MTM_FORMATION_DOES_NOT_EXIST;
        case LEAGUE_NO_FORMATIONS:
            return MTM_NO_FORMATIONS;
        case LEAGUE_FAN_ALREADY_EXIST:
            return MTM_FAN_ALREADY_EXISTS;
        case LEAGUE_FAN_DOES_NOT_EXIST:
            return MTM_FAN_DOES_NOT_EXIST;
        case LEAGUE_NO_FANS:
            return MTM_NO_FANS;
        case LEAGUE_FAN_ALREADY_IN_FORMATION:
            return MTM_FAN_ALREADY_IN_FORMATION;
        case LEAGUE_FAN_NOT_IN_FORMATION:
            return MTM_FAN_NOT_IN_FORMATION;
        case LEAGUE_INVALID_PARAMETERS:
            return MTM_INVALID_PARAMETERS;
        default:
            return MTM_OUT_OF_MEMORY;
    }
}


void HandleLeagueErrorMessage(LeagueResult result){
    mtmPrintErrorMessage(stderr, leagueResultToMtmErrorCodeConverter(result));
}


bool handleLeaguePrintReport(ReportType type,...){
    va_list ptr;
    switch (type) {
        case REPORT_FANS:
            va_start(ptr, type);
            char* fanName = va_arg(ptr, char*);
            int birthYear = va_arg(ptr, int);
            int id = va_arg(ptr, int);
            int formations = va_arg(ptr, int);
            mtmPrintFan(stdout, fanName, birthYear, id, formations);
            break;
        case REPORT_FORMATIONS:
            va_start(ptr, type);
            char* formationName = va_arg(ptr, char*);
            int formationSize = va_arg(ptr, int);
            mtmPrintFormation(stdout, formationName, formationSize);
            break;
        case REPORT_FORMATIONS_BY_AGE:
            va_start(ptr, type);
            int year = va_arg(ptr, int);
            const char** formationsNames = va_arg(ptr, const char**);
            int arrayLength = va_arg(ptr, int);
            mtmPrintFormationsByAge(stdout, year, formationsNames, arrayLength);
            break;
        default:
            va_end(ptr);
            return false;
    }
    va_end(ptr);
    return true;
}

bool beginParseAndExecute(FunctionParser parser){
    if(parser==NULL){
        return false;
    }
    LeagueResult result = LEAGUE_SUCCESS;
    char buffer[MAX_LEN]="";
    while(fgets(buffer, MAX_LEN, stdin)!=NULL){
        FunctionParserResult parserResult = functionParserParseStringAndExecute(parser, buffer,&result);
        if(parserResult==FUNCTION_PARSER_EMPTY_LINE||
           parserResult==FUNCTION_PARSER_MARKED_LINE ||
           parserResult==FUNCTION_PARSER_INVALID_COMMAND ||
           result == LEAGUE_NO_PRINT_CALLBACK){
            continue;
        }
        if(parserResult==FUNCTION_PARSER_FAILED_TO_EXECUTE){
            HandleLeagueErrorMessage(result);
            return false;
        }
        if(result!=LEAGUE_SUCCESS){
            HandleLeagueErrorMessage(result);
        }
        
        //Don't do anything on marked line or empty line and continue excetion
        
        
    }
    return true;
}

int main(int argc, const char * argv[])
{
    if(checkArguments(argc, argv)){
        League league = leagueCreate();
        if(league==NULL){
            mtmPrintErrorMessage(stderr, MTM_OUT_OF_MEMORY);
            return 0;
        }
        FunctionParser parser = functionParserCreate(league);
        if(parser==NULL){
            leagueDestroy(league);
            mtmPrintErrorMessage(stderr, MTM_OUT_OF_MEMORY);
            return 0;
        }
        leagueSetPrintReportFunction(league, handleLeaguePrintReport);
        
        beginParseAndExecute(parser);
        functionParserDestroy(parser);
        leagueDestroy(league);
        fclose(stdin);
        fclose(stdout);
    }
    
    return 0;
}

