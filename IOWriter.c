//
//  IOWriter.c
//  League
//
//  Created by Efi Shtain on 11/25/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//
//Test

#include <stdio.h>
#include <stdlib.h>
#include "IOWriter.h"
#include <stdarg.h>

struct ioWriter_t{
    League league;
};

bool handleLeaguePrintReport(ReportType type,...);

IOWriter IOWriterCreate(League league){
    if(league==NULL){
        return NULL;
    }
    IOWriter ioWriter = malloc(sizeof(*ioWriter));
    if(ioWriter==NULL){
        return NULL;
    }
    ioWriter->league=league;
    leagueSetPrintReportFunction(league, handleLeaguePrintReport);
    return ioWriter;
}

void IOWriterDestroy(IOWriter ioWriter){
    if(ioWriter!=NULL){
        free(ioWriter);
    }
}

static MtmErrorCode leagueResultToMtmErrorCodeConverter(LeagueResult result){
    switch (result) {
        case LEAGUE_FORMATION_ALREADY_EXIST:
            return MTM_FORMATION_ALREADY_EXISTS;
            
        case LEAGUE_FORMATION_NOT_EXIST:
            return MTM_FORMATION_DOES_NOT_EXIST;
        case LEAGUE_NO_FORMATIONS:
            return MTM_NO_FORMATIONS;
        case LEAGUE_FAN_ALREADY_EXIST:
            return MTM_FAN_ALREADY_EXISTS;
        case LEAGUE_FAN_DOES_NOT_EXIST:
            return MTM_FAN_DOES_NOT_EXIST;
        case LEAGUE_NO_FANS:
            return MTM_NO_FANS;
        case LEAGUE_FAN_ALREADY_IN_FORMATION:
            return MTM_FAN_ALREADY_IN_FORMATION;
        case LEAGUE_FAN_NOT_IN_FORMATION:
            return MTM_FAN_NOT_IN_FORMATION;
        case LEAGUE_INVALID_PARAMETERS:
            return MTM_INVALID_PARAMETERS;
        default:
            return MTM_OUT_OF_MEMORY;
    }
}


void HandleLeagueErrorMessage(LeagueResult result){
    mtmPrintErrorMessage(stderr, leagueResultToMtmErrorCodeConverter(result));
}


bool handleLeaguePrintReport(ReportType type,...){
    va_list ptr;
    switch (type) {
        case REPORT_FANS:
            va_start(ptr, type);
            char* fanName = va_arg(ptr, char*);
            int birthYear = va_arg(ptr, int);
            int id = va_arg(ptr, int);
            int formations = va_arg(ptr, int);
            mtmPrintFan(stdout, fanName, birthYear, id, formations);
            break;
        case REPORT_FORMATIONS:
            va_start(ptr, type);
            char* formationName = va_arg(ptr, char*);
            int formationSize = va_arg(ptr, int);
            mtmPrintFormation(stdout, formationName, formationSize);
            break;
        case REPORT_FORMATIONS_BY_AGE:
            va_start(ptr, type);
            int year = va_arg(ptr, int);
            const char** formationsNames = va_arg(ptr, const char**);
            int arrayLength = va_arg(ptr, int);
            mtmPrintFormationsByAge(stdout, year, formationsNames, arrayLength);
            break;
        default:
            va_end(ptr);
            return false;
    }
    va_end(ptr);
    return true;
}

IOWriterResult beginParseAndExecute(FunctionParser parser){
    if(parser==NULL){
        return IO_NULL_ARGUMENT;
    }
    LeagueResult result = LEAGUE_SUCCESS;
    char buffer[MAX_LEN]="";
    while(fgets(buffer, MAX_LEN, stdin)!=NULL){
        FunctionParserResult parserResult = functionParserParseStringAndExecute(parser, buffer,&result);
        if(parserResult==FUNCTION_PARSER_EMPTY_LINE||
           parserResult==FUNCTION_PARSER_MARKED_LINE ||
           parserResult==FUNCTION_PARSER_INVALID_COMMAND ||
           result == LEAGUE_NO_PRINT_CALLBACK){
            continue;
        }
        if(parserResult==FUNCTION_PARSER_FAILED_TO_EXECUTE){
            HandleLeagueErrorMessage(result);
            return IO_FAILED;
        }
        if(result!=LEAGUE_SUCCESS){
            HandleLeagueErrorMessage(result);
        }
        
        //Don't do anything on marked line or empty line and continue excetion
        
        
    }
    return IO_DONE;
}

IOWriterResult IOWriterBegin(IOWriter ioWriter){
    if(ioWriter==NULL){
        return IO_NULL_ARGUMENT;
    }
    FunctionParser parser = functionParserCreate(ioWriter->league);
    if(parser==NULL){
        mtmPrintErrorMessage(stderr, MTM_OUT_OF_MEMORY);
        return IO_MEMORY_ERROR;
    }
    
    IOWriterResult result =beginParseAndExecute(parser);
    functionParserDestroy(parser);
    return result;
}

