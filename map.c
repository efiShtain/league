//
//  map.c
//  mtm_ex2_map
//
//  Created by Efi Shtain on 11/11/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "map.h"

typedef struct keyValuePair *KeyValuePair;
struct keyValuePair{
    MapKeyElement key;
    MapDataElement data;
    KeyValuePair nextKey;
    KeyValuePair prevKey;
};

struct Map_t{
    int numberOfKeys;
    KeyValuePair head;
    KeyValuePair iterator;
    copyMapDataElements copyMapElement;
    copyMapKeyElements copyMapKey;
    freeMapDataElements freeMapElement;
    freeMapKeyElements freeMapKey;
    compareMapKeyElements compareMapKey;
};

Map mapCreate(copyMapDataElements copyDataElement, copyMapKeyElements copyKeyElement,
              freeMapDataElements freeDataElement, freeMapKeyElements freeKeyElement,
              compareMapKeyElements compareKeyElements){
    
    if (copyDataElement==NULL|| copyKeyElement==NULL || freeDataElement==NULL \
        || freeKeyElement==NULL||compareKeyElements==NULL) {
        return NULL;
    }
    Map map = malloc(sizeof(*map));
    if(map==NULL){
        return NULL;
    }
    map->head=NULL;
    map->iterator=NULL;
    map->copyMapElement=copyDataElement;
    map->copyMapKey=copyKeyElement;
    map->freeMapElement=freeDataElement;
    map->freeMapKey=freeKeyElement;
    map->compareMapKey=compareKeyElements;
    map->numberOfKeys=0;
    return map;
}

void freeKeyValuePair(KeyValuePair pair, freeMapKeyElements freeKey, freeMapDataElements freeData)
{
    if(pair!=NULL){
        freeKey(pair->key);
        pair->key=NULL;
        freeData(pair->data);
        pair->data=NULL;
        free(pair);
    }
}

//need to change the name so it will match the code conventions,
//this is not a verb
void recursiveDestroyMapKeyElements(KeyValuePair pair,freeMapKeyElements freeKey, freeMapDataElements freeData){
    if(pair==NULL){
        return;
    }
    
    if(pair!=NULL){
        recursiveDestroyMapKeyElements(pair->nextKey,freeKey,freeData);
    }
    freeKeyValuePair(pair,freeKey,freeData);
    pair=NULL;
    
}

void mapDestroy(Map map){
    if(map!=NULL){
        recursiveDestroyMapKeyElements(map->head,map->freeMapKey,map->freeMapElement);
        map->head=NULL;
        map->iterator=NULL;
        free(map);
        map=NULL;
    }
}

int mapGetSize(Map map){
    if(map==NULL){
        return -1;
    }
    return map->numberOfKeys;
}

KeyValuePair findByKey(MapKeyElement key, Map map){
    if(map->numberOfKeys==0){
        return NULL;
    }
    KeyValuePair keyPtr = map->head;
    while(keyPtr!=NULL){
        if(map->compareMapKey(key,keyPtr->key)==0){
            return keyPtr;
        }
        keyPtr=keyPtr->nextKey;
    }
    return NULL;
}

bool mapContains(Map map, MapKeyElement element){
    if(map==NULL || element==NULL){
        return false;
    }
    if(findByKey(element, map)!=NULL){
        return true;
    }
    return false;
}

KeyValuePair createNewKeyValuePair(Map map, MapKeyElement key, MapDataElement data){
    if(key==NULL||data==NULL){
        return NULL;
    }
    KeyValuePair newPair = malloc(sizeof(*newPair));
    if(newPair==NULL){
        return NULL;
    }
    newPair->key=map->copyMapKey(key);
    if(newPair->key==NULL){
        free(newPair);
        return NULL;
    }
    newPair->data=map->copyMapElement(data);
    if(newPair->data==NULL){
        map->freeMapKey(newPair->key);
        free(newPair);
    }
    newPair->nextKey=NULL;
    newPair->prevKey=NULL;
    return newPair;
}

MapResult addNewKeyToMap(Map map, MapKeyElement key, MapDataElement data){
    if(map==NULL||key==NULL||data==NULL){
        return MAP_NULL_ARGUMENT;
    }
    KeyValuePair pair = createNewKeyValuePair(map, key, data);
    if(pair==NULL){
        return MAP_OUT_OF_MEMORY;
    }
    
    if(map->head==NULL){
        map->head=pair;
        
    }else{
        KeyValuePair ptr = map->head;
        //first element is bigger than new element
        //so add new element as head, and head becomes second
        if(map->compareMapKey(ptr->key,pair->key)>0){
            pair->nextKey=ptr;
            ptr->prevKey=pair;
            map->head=pair;
        }else
        {
            int result =map->compareMapKey(ptr->key,pair->key);
            while (ptr->nextKey!=NULL&&result<0){
                ptr=ptr->nextKey;
                result=map->compareMapKey(ptr->key,pair->key);
                
            }
            if(result<0){
                ptr->nextKey=pair;
                pair->prevKey=ptr;
            }else{
                pair->nextKey=ptr;
                pair->prevKey=ptr->prevKey;
                ptr->prevKey=pair;
                pair->prevKey->nextKey=pair;
            }
            
            
        }
    }
    map->numberOfKeys++;
    return MAP_SUCCESS_ITEM_ADDED;
    
}

MapResult mapPut(Map map, MapKeyElement keyElement, MapDataElement dataElement){
    if(map==NULL||keyElement==NULL||dataElement==NULL){
        return MAP_NULL_ARGUMENT;
    }
    map->iterator=NULL;
    KeyValuePair pair=findByKey(keyElement, map);
    if(pair!=NULL){
        MapDataElement data = map->copyMapElement(dataElement);
        if(data==NULL){
            return MAP_OUT_OF_MEMORY;
        }
        map->freeMapElement(pair->data);
        pair->data=data;
        
        return MAP_SUCCESS;
    }
    MapResult result = addNewKeyToMap(map, keyElement, dataElement);
    return result;
    
}


MapResult mapRemove(Map map, MapKeyElement keyElement){
    if(map==NULL||keyElement==NULL){
        return MAP_NULL_ARGUMENT;
    }
    
    //Added for better performance, not really needed because if a pair doesn't exist
    if(map->numberOfKeys==0){
        return MAP_ITEM_DOES_NOT_EXIST;
    }
    
    KeyValuePair pair = findByKey(keyElement, map);
    if(pair==NULL){
        return MAP_ITEM_DOES_NOT_EXIST;
    }
    if(map->numberOfKeys==1){
        map->head->nextKey=NULL;
        map->head=NULL;
        
    }else{
        KeyValuePair prevKey = pair->prevKey;
        if(prevKey==NULL){ //remove head
            //            KeyValuePair temp=map->head;
            //            map->head=map->head->nextKey;
            //            map->head->prevKey=NULL;
            //            map->numberOfKeys--;
            //            map->iterator=NULL;
            //            freeKeyValuePair(temp, map->freeMapKey, map->freeMapElement);
            //            return MAP_SUCCESS;
            pair=map->head;
            map->head=map->head->nextKey;
            map->head->prevKey=NULL;
            
        }else{
            prevKey->nextKey=pair->nextKey;
            if(pair->nextKey!=NULL){
                pair->nextKey->prevKey=prevKey;
            }
        }
    }
    freeKeyValuePair(pair, map->freeMapKey, map->freeMapElement);
    map->numberOfKeys--;
    
    map->iterator=NULL;
    return MAP_SUCCESS;
    
}

MapResult mapClear(Map map){
    if(map==NULL){
        return MAP_NULL_ARGUMENT;
    }
    recursiveDestroyMapKeyElements(map->head, map->freeMapKey, map->freeMapElement);
    map->head=NULL;
    map->iterator=NULL;
    map->numberOfKeys=0;
    return MAP_SUCCESS;
}

bool copyMapsInternalList(Map sourceMap, Map destinationMap){
    KeyValuePair mapPtr = sourceMap->head;
    while(mapPtr!=NULL){
        MapResult result =mapPut(destinationMap, mapPtr->key, mapPtr->data);
        if(result!=MAP_SUCCESS_ITEM_ADDED){
            return result;
        }
        mapPtr=mapPtr->nextKey;
    }
    return true;
}

Map mapCopy(Map map) {
    if (map==NULL) {
        return NULL;
    }
    Map newMap = mapCreate(map->copyMapElement, map->copyMapKey, map->freeMapElement, map->freeMapKey, map->compareMapKey);
    if (newMap==NULL) {
        return NULL;
    }
    if(!copyMapsInternalList(map,newMap)){
        mapDestroy(newMap);
        return NULL;
    }
    return newMap;
}

MapKeyElement mapGetFirst(Map map, MapResult* result){
    if(map==NULL||map->head==NULL){
        *result=MAP_NULL_ARGUMENT;
        return NULL;
    }
    *result=MAP_SUCCESS;
    map->iterator=map->head;
    MapKeyElement key = map->copyMapKey(map->head->key);
    if(key==NULL){
        *result=MAP_OUT_OF_MEMORY;
    }
    return key;
}


MapKeyElement mapGetNext(Map map, MapResult* result){
    if(map==NULL||map->iterator==NULL||map->iterator->nextKey==NULL){
        *result=MAP_NULL_ARGUMENT;
        return NULL;
    }
    *result=MAP_SUCCESS;
    map->iterator=map->iterator->nextKey;
    MapKeyElement key =map->copyMapKey(map->iterator->key);
    if(key==NULL){
        *result=MAP_OUT_OF_MEMORY;
    }
    return key;
    
}

MapDataElement mapGet(Map map, MapKeyElement keyElement){
    if(map==NULL||keyElement==NULL){
        return NULL;
    }
    KeyValuePair pair = findByKey(keyElement, map);
    if(pair==NULL){
        return NULL;
    }
    MapDataElement data = map->copyMapElement(pair->data);
    
    return data;
}




