
//
//  league.h
//  League
//
//  Created by Efi Shtain on 11/14/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#ifndef League_league_h
#define League_league_h
#include <stdbool.h>

/**
 * Basic league
 *
 * Implements a basic league, holding formations and fans which belong to league ,
 * also has printing function
 *
 * The following functions are available:
 *
 *   leagueCreate          			  - Creates a new league
 *   leagueDestroy        			  - Deletes an existing league and frees all resources
 *   leagueSetPrintReportFunction     - set the function to print from
 *   leagueAddFormation		 		  - add formation to league
 *   leagueRemoveFormation			  - remove formation from league
 *   leagueReportFormations 		  - reports on formation in league
 *   leagueAddFan      				  - add fan to league
 *	 leagueRemoveFan 				  - remove fan from league and from all the formations he support
 *   leagueReportFans         		  - report about fans in the league
 *   leagueAddFanToFormation   		  - add fan from league to support formation
 *   leagueRemoveFanFromFormation	  - remove fan from supporting formation
 *   leagueReportFormationsByAge	  - reports on fans in league by age
 */

/** Type for defining the league */
typedef struct league_t *League;

/** Type used for returning error codes from league functions */
typedef enum {
    LEAGUE_SUCCESS,
    LEAGUE_NULL_ARGUMENT,
    LEAGUE_MEMEORY_ERROR,
    LEAGUE_FORMATION_ALREADY_EXIST,
    LEAGUE_FORMATION_NOT_EXIST,
    LEAGUE_NO_FORMATIONS,
    LEAGUE_FAN_ALREADY_EXIST,
    LEAGUE_FAN_DOES_NOT_EXIST,
    LEAGUE_NO_FANS,
    LEAGUE_FAN_ALREADY_IN_FORMATION,
    LEAGUE_FAN_NOT_IN_FORMATION,
    LEAGUE_INVALID_PARAMETERS,
    LEAGUE_NO_PRINT_CALLBACK
}LeagueResult;


/** Type used for print function callback function */
typedef enum{
    REPORT_FORMATIONS,
    REPORT_FANS,
    REPORT_FORMATIONS_BY_AGE
}ReportType;

/** type of print function callback */
typedef bool (*PrintFunc)(ReportType,...);


/**
 * leagueCreate: Creates a new league
 *
 * @return
 * 	NULL - if the allocations failed.
 * 	A new league in case of success.
 */
League leagueCreate();

/**
 * leagueDestroy: Deletes an existing league and frees all its resources
 *
 * @param league - Target league to be deallocated. If league is NULL nothing will be
 * 		done
 */
void leagueDestroy(League);

// TODO : a report on following func
LeagueResult leagueSetPrintReportFunction(League,PrintFunc);

/**
 * leagueAddFormation : add formation to relevant league
 *
 * @param league - target league to add the formation to
 * @param char* - name of formation to be add
 * @return
 * LEAGUE_NULL_ARGUMENT - if NULL was sent
 * LEAGUE_FORMATION_ALREADY_EXIST - if  formation with the same name already exists
 * LEAGUE_MEMEORY_ERROR - on any memory allocation failure
 * LEAGUE_SUCCESS - is success
 */
LeagueResult leagueAddFormation(League,char*);

/**
 * leagueRemoveFormation : remove formation from league and all
 * fans who support does not support him anymore,
 * frees all its resources
 *
 * @param league - relevant league
 * @param char*  - formation name to be remove
 * @return
 * LEAGUE_NULL_ARGUMENT - if NULL was sent
 * LEAGUE_FORMATION_NOT_EXIST - if  formation with this  name not exists in league
 * LEAGUE_SUCCESS - otherwise
 */
LeagueResult leagueRemoveFormation(League,char*);


/**
 * leagueReportFormations : prints the formations with the biggest number
 * of fans in relevant league
 * formations will be printed according to the number param
 * and will print in descending order according to their number
 * of fans. in case of equality in up lexicographic order
 *
 * @param league - relevant league
 * @param int - number of formation to print, if the number of formations
 * to print is bigger from numbers of formation in league all formations in
 * league will be printed
 * @return
 * LEAGUE_NULL_ARGUMENT - if NULL was sent
 * LEAGUE_NO_FORMATIONS - if ther are no formations in relevant league
 * LEAGUE_MEMEORY_ERROR - on any memory allocation failure
 * LEAGUE_SUCCESS - in any other case
 */
LeagueResult leagueReportFormations(League, int);

/**
 * leagueAddFan : add fan to be a fan in league
 *
 * @param league - the relevant league
 * @param long - ID of the fan
 * @param char* - name of fan
 * @param int - the fan year of birth
 * @return
 * LEAGUE_NULL_ARGUMENT - if NULL was sent
 * LEAGUE_INVALID_PARAMETERS - if year of birth is invalid (0-2013 is valid)
 * LEAGUE_FAN_ALREADY_EXIST - fan already belongs to league\
 * LEAGUE_MEMEORY_ERROR - on any memory allocation failure
 * LEAGUE_SUCCESS - fan added
 */
LeagueResult leagueAddFan(League, long,char*,int);

/**
 * leagueRemoveFan : remove fan from league and from all the formation
 * he support, and frees all its resources
 *
 * @param league - relevant league
 * @param long - fan ID
 * @return
 * LEAGUE_NULL_ARGUMENT - if NULL was sent
 * LEAGUE_FAN_DOES_NOT_EXIST - if the fan with given id not found
 * LEAGUE_SUCCESS - fan removed
 *
 */
LeagueResult leagueRemoveFan(League, long);

// TODO : check it again
/**
 * leagueReportFans : prints the list fan in league, the list will be
 * sorted in up order of fans id
 *
 * @return
 * LEAGUE_NULL_ARGUMENT - if NULL was sent
 * LEAGUE_NO_FANS - if ther are no fans in league
 * LEAGUE_SUCCESS - if success
 */
LeagueResult leagueReportFans(League);

/**
 * leagueAddFanToFormation : joins fan to target formation
 *
 * @param league - relevant league
 * @param char* - formation name to be join to
 * @param long - the fan id to join
 * @return
 * LEAGUE_NULL_ARGUMENT - if NULL was sent
 * LEAGUE_INVALID_PARAMETERS - if fan id is invalid
 * LEAGUE_FORMATION_NOT_EXIST - if formation not exists in league
 * LEAGUE_FAN_DOES_NOT_EXIST - if fan is not exists in league
 * LEAGUE_FAN_ALREADY_IN_FORMATION - if fan already support
 * target formation
 * LEAGUE_SUCCESS - is success
 *
 */
LeagueResult leagueAddFanToFormation(League, char*,long);

/**
 * leagueRemoveFanFromFormation : remove fan from target formation
 *
 * @param league - relevant league
 * @param char* - formation name to be remove from
 * @param long - the fan id who leave formation
 * @return
 * LEAGUE_NULL_ARGUMENT - if NULL was sent
 * LEAGUE_FORMATION_NOT_EXIST - if formation not exists in league
 * LEAGUE_FAN_DOES_NOT_EXIST - if fan is not exists in league
 * LEAGUE_FAN_NOT_IN_FORMATION - if fan not support target formation
 * LEAGUE_SUCCESS - is success
 *
 */
LeagueResult leagueRemoveFanFromFormation(League, char*,long);


/**
 * leagueReportFormationsByAge : prints a report of formations in leaue,
 * ordered by ages of fans supporting them.
 * The report is printed ordered by age from youngest to oldest
 * and for each age, the list of formations is ordered by lexicographic order
 * @param league - relevant league
 * @return
 * LEAGUE_NULL_ARGUMENT - if NULL was sent
 * LEAGUE_NO_FORMATIONS - if no formation in league
 * LEAGUE_NO_FANS - if  no fans in league
 */
LeagueResult leagueReportFormationsByAge(League);
#endif
