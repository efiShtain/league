#include "list.h"
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include "verify.h"

/*!
 * Macros for checking and retrieving error codes
 */
#define VERIFY_NOT_NULL(ptr) VERIFY(ptr,LIST_NULL_ARGUMENT)
#define VERIFY_CURRENT_VALID(list) VERIFY(list->current,LIST_INVALID_CURRENT)
#define VERIFY_ALLOCATION(ptr) VERIFY(ptr,LIST_OUT_OF_MEMORY)
#define VERIFY_ALLOCATION_RETURN_PTR(ptr) VERIFY(ptr,NULL)

#define FOREACH(ptr,list) \
	for(Node ptr = list->head->next ; ptr ; ptr = ptr->next)

/*!
 * Struct used for a node of a linked list
 */
typedef struct Node_t {
	ListElement data;
	struct Node_t* next;
} *Node;

static Node nodeCreate(const ListElement data, CopyListElement copyElement) {
	assert(copyElement!=NULL || data == NULL);
	Node ptr = malloc(sizeof(struct Node_t));
	if (!ptr) {
		return NULL;
	}
	ptr->next = NULL;
	if (!data) {
		ptr->data = NULL;
		return ptr;
	}
	ptr->data = copyElement(data);
	if (!data) {
		free(ptr);
		return NULL;
	}
	return ptr;
}

static void nodeDestroy(Node node, FreeListElement freeElement) {
	if (!node) {
		return;
	}

	assert(freeElement != NULL);
	freeElement(node->data);
	free(node);
}

static Node nodeFindParent(Node node, Node searchStartNode) {
	assert (node != NULL && searchStartNode != NULL);
	Node iterator = searchStartNode;
	while(iterator && iterator->next != node) {
		iterator = iterator->next;
	}
	assert (iterator->next == node);
	return iterator;
}

/**
 * Struct used for a linked list
 */
struct List_t {
	Node head;
	Node current;
	int size;
	CopyListElement copyElement;
	FreeListElement freeElement;
};

List listCreate(CopyListElement copyElement, FreeListElement freeElement) {
	if (!copyElement || !freeElement) {
		return NULL;
	}

	List ptr = malloc(sizeof(struct List_t));
	if (!ptr) {
		return NULL;
	}

	ptr->head = nodeCreate(NULL, NULL);
	if (!ptr->head) {
		free(ptr);
		return NULL;
	}
	ptr->current = NULL;
	ptr->size = 0;
	ptr->copyElement = copyElement;
	ptr->freeElement = freeElement;
	return ptr;
}

List listCopy(List list) {
	if (!list) {
		return NULL;
	}
	assert(list->copyElement);
	assert(list->freeElement);

	List newList = listCreate(list->copyElement,list->freeElement);
	VERIFY_ALLOCATION_RETURN_PTR(newList);
	newList->current = newList->head;
	Node current = NULL;
	FOREACH(ptr,list) {
		ListResult listResult = listInsertAfterCurrent(newList, ptr->data);
		if (listResult == LIST_OUT_OF_MEMORY) {
			listDestroy(newList);
			return NULL;
		}

		assert(listResult == LIST_SUCCESS);
		listGetNext(newList);
		if (ptr == list->current) {
		    current = newList->current;
		}
	}
	newList->current = current;
	return newList;
}

List listFilter(List list, FilterListElement filterElement) {
	if (!list || !filterElement) {
		return NULL;
	}
	assert(list->copyElement);
	assert(list->freeElement);

	List newList = listCreate(list->copyElement, list->freeElement);
	VERIFY_ALLOCATION_RETURN_PTR(newList);
	newList->current = newList->head;
	FOREACH(ptr, list) {
		if (filterElement(ptr->data)) {
			ListResult listResult = listInsertLast(newList, ptr->data);
			if (listResult == LIST_OUT_OF_MEMORY) {
				listDestroy(newList);
				return NULL;
			}
		}
	}
	return newList;
}

int listGetSize(List list) {
	return list ? list->size : -1;
}

ListElement listGetFirst(List list) {
	if (!list) {
		return NULL;
	}
	list->current = list->head->next;
	return list->current ? list->current->data : NULL;
}

ListElement listGetNext(List list) {
	if (!list || !list->current) {
		return NULL;
	}
	list->current = list->current->next;
	return list->current ? list->current->data : NULL;
}

ListResult listInsertFirst(List list, ListElement element) {
	VERIFY_NOT_NULL(list);
	Node newNode = nodeCreate(element, list->copyElement);
	VERIFY_ALLOCATION(newNode);
	newNode->next = list->head->next;
	list->head->next = newNode;
	list->size++;
	return LIST_SUCCESS;
}

ListResult listInsertLast(List list, ListElement element) {
	VERIFY_NOT_NULL(list);
	Node newNode = nodeCreate(element, list->copyElement);
	VERIFY_ALLOCATION(newNode);
	Node ptr = list->head;
	while(ptr->next) {
		ptr = ptr->next;
	}
	ptr->next = newNode;
	list->size++;
	return LIST_SUCCESS;
}

ListResult listInsertBeforeCurrent(List list, ListElement element) {
	VERIFY_NOT_NULL(list);
	VERIFY_CURRENT_VALID(list);
	Node parent = nodeFindParent(list->current,list->head);
	assert(parent != NULL);
	Node newNode = nodeCreate(element, list->copyElement);
	VERIFY_ALLOCATION(newNode);
	newNode->next = parent->next;
	parent->next = newNode;
	list->size++;
	return LIST_SUCCESS;
}

ListResult listInsertAfterCurrent(List list, ListElement element) {
	VERIFY_NOT_NULL(list);
	VERIFY_CURRENT_VALID(list);
	Node newNode = nodeCreate(element, list->copyElement);
	VERIFY_ALLOCATION(newNode);
	newNode->next = list->current->next;
	list->current->next = newNode;
	list->size++;
	return LIST_SUCCESS;
}

ListResult listRemoveCurrent(List list) {
	VERIFY_NOT_NULL(list);
	VERIFY_CURRENT_VALID(list);
	Node parent = nodeFindParent(list->current,list->head);
	assert(parent != NULL);
	Node temp = parent->next;
	parent->next = temp->next;
	nodeDestroy(temp, list->freeElement);
	list->current = NULL;
	list->size--;
	return LIST_SUCCESS;
}

ListElement listGetCurrent(List list) {
	if (!list || !list->current) {
		return NULL;
	}
	return list->current->data;
}

/*
 * Sorting by moving the list into an array,
 * that is because quicksort on a linked list requires a bidirectional list
 * And no, the standard qsort function does not work due to different compare
 * function prototype.
 */
static void swap(void** a, void** b) {
	void* temp = *a;
	*a = *b;
	*b = temp;
}

static int partition(void** array, int size, CompareListElements compare) {
   swap(&array[0],&array[size - 1]);
   void* pivot = array[size - 1];
   int storeIndex = 0;
   for(int i=0; i<size - 1 ; i++) {
	   if (compare(array[i],pivot) <= 0) {
		   swap(&array[i],&array[storeIndex++]);
	   }
   }
   swap(&array[storeIndex],&array[size - 1]);
   return storeIndex;
}

static void quicksort(void** array, int size, CompareListElements compare) {
	assert(compare != NULL && array != NULL);
	if (size <= 1) {
		return;
	}
	int pivotIndex = partition(array,size,compare);
	quicksort(array, pivotIndex, compare);
	quicksort(array + pivotIndex + 1, size - pivotIndex - 1, compare);
}

ListResult listSort(List list, CompareListElements compareElement) {
	VERIFY_NOT_NULL(list);
	VERIFY_NOT_NULL(compareElement);
	ListElement* array = malloc(sizeof(ListElement)*list->size);
	int i=0;
	VERIFY_ALLOCATION(array);
	for(Node ptr = list->head->next; ptr ; ptr=ptr->next) {
		array[i++] = ptr->data;
	}
	quicksort(array,list->size,compareElement);
	i=0;
	for(Node ptr = list->head->next; ptr ; ptr=ptr->next) {
		ptr->data = array[i++];
	}
	free(array);
	return LIST_SUCCESS;
}

ListResult listClear(List list) {
	VERIFY_NOT_NULL(list);
	while(list->size > 0) {
		listGetFirst(list);
		listRemoveCurrent(list);
	}
	return LIST_SUCCESS;
}

void listDestroy(List list) {
	if (!list) {
		return;
	}

	Node ptr = list->head;
	while(ptr) {
		Node temp = ptr;
		ptr = ptr->next;
		nodeDestroy(temp,list->freeElement);
	}
	free(list);
}

