/*
 * verify.h
 *
 *  Created on: 05/03/2010
 *      Author: Strulovich
 */

#ifndef VERIFY_H_
#define VERIFY_H_

#define VERIFY(condition, error) do { \
	if (!(condition)) return error; \
	} while(0)

#endif /* VERIFY_H_ */
