/*

 * tests.c
 *
 *  Created on: Dec 13, 2013
 *      Author: Yakir
 */





#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "fan.h"

#define ASSERT(expr) do { \
	if(!(expr)) { \
		printf("\nAssertion failed %s (%s:%d).\n", #expr, __FILE__, __LINE__); \
		return false; \
	} else { \
		printf("."); \
	} \
} while (0)

#define RUN_TEST(test) do { \
  printf("Running "#test); \
  if(test()) { \
    printf("[OK]\n"); \
  } \
} while(0)

#define ASSERT_NULL_ARGUMENT(expr) ASSERT((expr) == NULL)
#define ASSERT_NOT_NULL(expr) ASSERT(expr != NULL)
#define ASSERT_CORRECT_INT(expr1,expr2) ASSERT (expr1==expr2)
#define ASSERT_CORRECT_BIRTHE(expr1,expr2) ASSERT (expr1==expr2)
#define ASSERT_CORRECT_ID(expr1,expr2) ASSERT (expr1==expr2)
//#define ASSERT_TRUE(expr) ASSERT((expr) == true)
//#define ASSERT_FALSE(expr) ASSERT((expr) == false)


/*
 * testFanCreate : checking fan create fun
 * check regular create and create with adges cases
 */
static bool testFanCreate(void) {
	// qustion to efi is ID 0 is good?
	// strange thing. eclipse asked me to do fan destroy
	Fan fan = fanCreate(-1,"yakir",2000);
	ASSERT_NULL_ARGUMENT(fan);
	fanDestroy(fan);
	Fan fan1 = fanCreate(4,NULL,2000);
	ASSERT_NULL_ARGUMENT(fan1);
	fanDestroy(fan1);
	Fan fan2 = fanCreate(4,"yakir",-99);
	ASSERT_NULL_ARGUMENT(fan2);
	fanDestroy(fan2);
	Fan fan3 = fanCreate(4,"yakir",2090);
	ASSERT_NULL_ARGUMENT(fan3);
	fanDestroy(fan3);

	//checking adges
	Fan fan11 = fanCreate(4,"yakir",-1);
	ASSERT_NULL_ARGUMENT(fan11);
	Fan fan0 = fanCreate(3,NULL,2014);
	ASSERT_NULL_ARGUMENT(fan0);
	Fan fan8 = fanCreate(4,"efi",0);
	ASSERT_NOT_NULL(fan8);
	fanDestroy(fan8);
	Fan fan9 = fanCreate(4,"efi",2013);
	ASSERT_NOT_NULL(fan9);
	fanDestroy(fan9);

	Fan fan4 = fanCreate(4,"yakir",2000);
	ASSERT_NOT_NULL(fan4);
	//check correct value
	ASSERT_CORRECT_ID(fanGetId(fan4),4);
	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan4),2000);
	ASSERT_CORRECT_INT(strcmp("yakir",fanGetName(fan4)),0);
	fanDestroy(fan4);

	Fan fan5 = fanCreate(1,"moni",2013);
	ASSERT_NOT_NULL(fan5);
	//check correct value
	ASSERT_CORRECT_ID(fanGetId(fan5),1);
	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan5),2013);
	ASSERT_CORRECT_INT(strcmp("moni",fanGetName(fan5)),0);
	fanDestroy(fan5);

	Fan fan6 = fanCreate(435,"moni",0);
	ASSERT_NOT_NULL(fan6);
	//check correct value
	ASSERT_CORRECT_ID(fanGetId(fan6),435);
	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan6),0);
	ASSERT_CORRECT_INT(strcmp("moni",fanGetName(fan6)),0);
	fanDestroy(fan6);

	Fan fan7 = fanCreate(422,"efi",1900);
	ASSERT_NOT_NULL(fan7);
	//check correct value
	ASSERT_CORRECT_ID(fanGetId(fan7),422);
	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan7),1900);
	ASSERT_CORRECT_INT(strcmp("efi",fanGetName(fan7)),0);
	fanDestroy(fan7);

	return true;
}

static bool testFanDestroy (void){
	// i think that in the function herself after free name needed to be assert null
	Fan fan = fanCreate(4,"yakir",2000);
	ASSERT_NOT_NULL(fan);
	fanDestroy(fan);
	// need to check if SADOT are still exists?
	//ASSERT_NULL_ARGUMENT(fan);

	return true;
}

/*
 * testFanCopy : testing if the value of the copy are correct
 * also if the copy deleted so original fan will remain the same
 */
static bool testFanCopy(void){

	ASSERT_NULL_ARGUMENT(fanCopy(NULL));
	Fan fan = fanCreate(4,"yakir",2000);
	ASSERT_NOT_NULL(fan);
	Fan copyFan = fanCopy(fan);
	ASSERT_NOT_NULL(copyFan);
	ASSERT_CORRECT_ID(fanGetId(copyFan),4);
	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(copyFan),2000);
	ASSERT_CORRECT_INT(strcmp("yakir",fanGetName(copyFan)),0);
	fanDestroy(copyFan);
	ASSERT_NOT_NULL(fan);
	ASSERT_CORRECT_ID(fanGetId(fan),4);
	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan),2000);
	ASSERT_CORRECT_INT(strcmp("yakir",fanGetName(fan)),0);
	fanDestroy(fan);

	ASSERT_NULL_ARGUMENT(fanCopy(NULL));
	Fan fan1 = fanCreate(300,"efi",2);
	ASSERT_NOT_NULL(fan1);
	Fan copyFan1 = fanCopy(fan1);
	ASSERT_NOT_NULL(copyFan1);
	ASSERT_CORRECT_ID(fanGetId(copyFan1),300);
	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(copyFan1),2);
	ASSERT_CORRECT_INT(strcmp("efi",fanGetName(copyFan1)),0);
	fanDestroy(copyFan1);
	ASSERT_NOT_NULL(fan1);
	ASSERT_CORRECT_ID(fanGetId(fan1),300);
	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan1),2);
	ASSERT_CORRECT_INT(strcmp("efi",fanGetName(fan1)),0);
	fanDestroy(fan1);


	return true;

}

/*
 * testFanGetId: check if we get correct id
 */
static bool testFanGetId(void) {
	ASSERT(fanGetId(NULL)==-1);
	Fan fan = fanCreate(4,"yakir",2000);
	ASSERT_NOT_NULL(fan);
	ASSERT_CORRECT_ID(fanGetId(fan),4);
	fanDestroy(fan);

	Fan fan1 = fanCreate(0,"yakir",2000);
	ASSERT_NOT_NULL(fan1);
	ASSERT_CORRECT_ID(fanGetId(fan1),0);
	fanDestroy(fan1);

	Fan fan2 = fanCreate(993783,"yakir",2000);
	ASSERT_NOT_NULL(fan2);
	ASSERT_CORRECT_ID(fanGetId(fan2),993783);
	fanDestroy(fan2);

	return true;

}

/*
 * testFanGetName : check if the copy name we recived is ok
 * and also if we change the copy so original fan name
 * should not be changed
 */
static bool testFanGetName(void){
	ASSERT_NULL_ARGUMENT(fanGetName(NULL));
	Fan fan = fanCreate(100,"moshe_1",2006);
	ASSERT_NOT_NULL(fan);
	char* copyName = fanGetName(fan);
	ASSERT_CORRECT_INT(strcmp(copyName,fanGetName(fan)),0);
	copyName[3]='o';
	// do we need to check if we change fan name so the copy does not changed?
	ASSERT_CORRECT_INT(strcmp(copyName,"mosoe_1"),0);
	ASSERT_CORRECT_INT(strcmp("moshe_1",fanGetName(fan)),0);
	free (copyName);
	fanDestroy(fan);
	return true;
}

/*
 * testFanDateOfBirth : check if the copy DateOfBirth we recived is ok
 * and also if we change the copy so original fan DateOfBirth
 * should not be changed
 */
static bool testFanGetDateOfBirth(void) {
	ASSERT_CORRECT_INT(fanGetDateOfBirth(NULL),-1);
	Fan fan = fanCreate(4,"yakir",2000);
	ASSERT_NOT_NULL(fan);
	int year = fanGetDateOfBirth(fan);
	ASSERT_CORRECT_BIRTHE(2000,year);
	year = 8989;
	ASSERT_CORRECT_BIRTHE(fanGetDateOfBirth(fan),2000);
	fanDestroy(fan);
	// there are two functions of fanGetDateOfBirth?? 1 gets void
	return true;
}

/*
 * testFanCompareFansByAge : assert the code error -2 is really returned
 * and that the value of return are correct
 */
static bool testFanCompareFansByAge(void){
	Fan fan1 = fanCreate(4,"yakir",2000);
	ASSERT_NOT_NULL(fan1);
	Fan fan2 = fanCreate(5,"efi",2001);
	ASSERT_NOT_NULL(fan2);
	Fan fan3 = fanCreate(5,"mefi",2001);
	ASSERT_NOT_NULL(fan3);
	ASSERT_CORRECT_INT(fanCompareFansByAge(NULL,fan1),-2);
	ASSERT_CORRECT_INT(fanCompareFansByAge(fan2,NULL),-2);
	ASSERT_CORRECT_INT(fanCompareFansByAge(NULL,NULL),-2);
	ASSERT_CORRECT_INT(fanCompareFansByAge(fan1,fan2),-1);
	ASSERT_CORRECT_INT(fanCompareFansByAge(fan2,fan1),1);
	ASSERT_CORRECT_INT(fanCompareFansByAge(fan2,fan3),0);
	fanDestroy(fan1);
	fanDestroy(fan3);
	fanDestroy(fan2);
	return true;
}

/*
 * testFanCompareFansById : assert the code error -2 is really returned
 * and that the value of return are correct
 */
static bool testFanCompareFansById(void) {
	Fan fan1 = fanCreate(4,"yakir",2000);
	ASSERT_NOT_NULL(fan1);
	Fan fan2 = fanCreate(5,"efi",2001);
	ASSERT_NOT_NULL(fan2);
	Fan fan3 = fanCreate(5,"mefi",2001);
	ASSERT_NOT_NULL(fan3);
	ASSERT_CORRECT_INT(fanCompareFansByAge(NULL,fan1),-2);
	ASSERT_CORRECT_INT(fanCompareFansByAge(fan2,NULL),-2);
	ASSERT_CORRECT_INT(fanCompareFansByAge(NULL,NULL),-2);
	ASSERT_CORRECT_INT(fanCompareFansByAge(fan2,fan1),1);
	ASSERT_CORRECT_INT(fanCompareFansByAge(fan1,fan2),-1);
	ASSERT_CORRECT_INT(fanCompareFansByAge(fan2,fan3),0);
	fanDestroy(fan1);
	fanDestroy(fan3);
	fanDestroy(fan2);
	return true;
}
/*
int main(void) {
	RUN_TEST(testFanCompareFansById);// ok
	RUN_TEST(testFanCompareFansByAge);//ok
	RUN_TEST(testFanGetDateOfBirth);//ok
	RUN_TEST(testFanGetId);// Assertion failed (fanGetId(fan1))==4
	RUN_TEST(testFanCreate);// Assertion failed (strcmp("yakir",fanGetName(fan5)))==0
	RUN_TEST(testFanDestroy);//Assertion failed (fan) == NULL
	RUN_TEST(testFanCopy);// ok
	RUN_TEST(testFanGetName);// ok

	return 0;
}
*/

