//
//  fan.c
//  Ex2Mtm
//
//  Created by Efi Shtain on 11/13/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#include <stdio.h>
#include "fan.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


//consider defining all of the arguments as const
struct fan_t {
    long id;
    char* name;
    int yearOfBirth;
};

static bool checkDateOfBirth(int dateOfBirth){
    if(dateOfBirth<MIN_YEAR_OF_BIRTH||dateOfBirth>MAX_YEAR_OF_BIRTH){
        return false;
    }
    return true;
}

Fan fanCreate(long id, char* name, int yearOfBirth){
    if(id<0 || name==NULL|| !checkDateOfBirth(yearOfBirth)){
        return NULL;
    }
    Fan newFan = malloc(sizeof(*newFan));
    if(newFan==NULL){
        return NULL;
    }
    newFan->name=malloc(strlen(name)+1);
    if(newFan->name==NULL){
        free(newFan);
        newFan=NULL;
        return NULL;
    }
    newFan->id=id;
    strcpy(newFan->name,name);
    newFan->yearOfBirth=yearOfBirth;
    
    return newFan;
}

void fanDestroy(Fan fan){
    if(fan!=NULL){
        free(fan->name);
        fan->name=NULL;
        free(fan);
        fan=NULL;
    }
}

Fan fanCopy(Fan fan){
    if(fan==NULL){
        return NULL;
    }
    
    return fanCreate(fan->id, fan->name, fan->yearOfBirth);
}


int fanCompareFansByAge(Fan fanA,Fan fanB){
	if(fanA==NULL||fanB==NULL){
        return -2;
    }
    if(fanA->yearOfBirth>fanB->yearOfBirth){
        return 1;
    }
    if(fanA->yearOfBirth<fanB->yearOfBirth){
        return -1;
    }
    return 0;
}

int fanGetDateOfBirth(Fan fan){
	if(fan!=NULL){
		return fan->yearOfBirth;
	}
	return -1;
}

char* fanGetName(Fan fan){
	if (fan==NULL) {
		return NULL;
	}
	char* nameCopy = malloc(strlen(fan->name)+1);
	if (nameCopy==NULL){
		return NULL;
	}
	return strcpy(nameCopy,fan->name);
}

long fanGetId(Fan fan){
    if(fan==NULL){
        return -1;
    }
    return fan->id;
}

int fanCompareFansById(Fan fanA, Fan fanB){
    if(fanA==NULL||fanB==NULL){
        return -2;
    }
    if(fanA->id>fanB->id){
        return 1;
    }
    if(fanA->id<fanB->id){
        return -1;
    }
    return 0;
}
