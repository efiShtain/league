//
//  functionParser.h
//  League
//
//  Created by Efi Shtain on 11/14/13.
//  Copyright (c) 2013 Efi Shtain. All rights reserved.
//

#ifndef League_functionParser_h
#define League_functionParser_h
#include "league.h"

/**
 * Function Parser
 *
 * Implements a basic league functions parser
 * Parser converts a string command to one of League's interface functions and call is using the parsed parameters
 *
 * The following functions are available:
 *
 *   functionParserCreate                 - Creates a new function parser
 *   functionParserDestroy                - Deletes an existing parser and frees all resources
 *   functionParserParseStringAndExecute  - Parse a string and execute the relevant league function
 */

/**
 * Delemiters by which string is parsed
 */
#define STRTOK_DELIMITERS "\t \n\r"
/**
 * Marks a comment line
 */
#define MARKED_LINE_IDENTIFIER '#'
/**
 * Maximum number of arguments per command
 */
#define MAX_ARGUMENTS_PER_COMMAND 5


/** Type for defining the function parser */
typedef struct functionParser_t *FunctionParser;

/** Type used for returning error codes from function parser's functions */
typedef enum{
    FUNCTION_PARSER_SUCCESS,
    FUNCTION_PARSER_MEMEORY_ERROR,
    FUNCTION_PARSER_NULL_ARGUMENT,
    FUNCTION_PARSER_INVALID_COMMAND,
    FUNCTION_PARSER_MARKED_LINE,
    FUNCTION_PARSER_EMPTY_LINE,
    FUNCTION_PARSER_FAILED_TO_EXECUTE,
    FUNCTION_PARSER_VALID_COMMAND
}FunctionParserResult;

/**
 * functionParserCreate: Creates a new league function parser
 *
 * @param League - The league which function's shall be called
 * @return
 * 	NULL - if one of the parameters is NULL or allocations failed.
 * 	A new function parser in case of success.
 */
FunctionParser functionParserCreate(League);

/**
 * functionParserDestroy: Deletes an existing functionParser and frees all its resources
 *
 * @param FunctionParser - Target FunctionParser to be deallocated. If FunctionParser is NULL nothing will be
 * 		done
 */
void functionParserDestroy(FunctionParser);

/**
 * functionParserParseStringAndExecute: Parse a string and execute the relevant league function
 *
 * @param FunctionParser - The parser which holds the relevant league
 * @param char* - The string which is the command to be parsed and executed
 * @LeagueResult* - A pointer which the league result of the parsed action will be set
 * @return
 * FUNCTION_PARSER_MEMEORY_ERROR - is memory allocation failed
 * FUNCTION_PARSER_NULL_ARGUMENT - if any null argument was passed
 * FUNCTION_PARSER_INVALID_COMMAND - if the first parsed word is not a known command type
 * FUNCTION_PARSER_MARKED_LINE - if the string passed is a marked line (indicated by MARKED_LINE_IDENTIFIER as first char of string)
 * FUNCTION_PARSER_EMPTY_LINE - if the string passed is an empty line
 * FUNCTION_PARSER_FAILED_TO_EXECUTE - If function was called correcly but failed for any reason
 * FUNCTION_PARSER_SUCCESS - In case of success
 */
FunctionParserResult functionParserParseStringAndExecute(FunctionParser,char*,LeagueResult*);

#endif
